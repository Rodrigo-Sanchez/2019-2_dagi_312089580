import * as Koch from './koch.js';

export default class Developing1 {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.ctx = this.canvas.getContext("2d");

    this.right = this.canvas.width;
    this.bottom = this.canvas.height;

    this.level = 0;
    this.segments = [1, 4, 16, 64, 256, 1024];
    this.size = [1.000000, 0.333333, 0.111111, 0.037037, 0.012346, 0.004115];
    this.large = [1.000000, 1.333333, 1.777778, 2.370370, 3.160494, 4.213992];
    this.show = true;
  }

  /**
   * 
   */
  processInput() {
    let generationIn = document.getElementById("generationIn");
    let generationOut = document.getElementById("generationOut");
    if(generationIn !== null) {
      generationIn.onclick = () => {
        if (this.level >= 0 && this.level < 5) {
          this.level++;
        }
      };
    }
    if(generationOut !== null) {
      generationOut.onclick = () => {
        if (this.level > 0 && this.level <= 5) {
          this.level--;
        }
      };
    }
  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {
    if (this.show && this.level == 5) {
      $('.section1').load("./scenes/3-1_desarrollo-1.html");
      this.show = false;
    }
  }

  /**
   * 
   */
  render() {
    this.ctx.fillStyle = "black";
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.lineWidth = 0.5;
    for (let x = 0.5; x < 500; x += 50) {
      this.ctx.moveTo(x, 0);
      this.ctx.lineTo(x, 500);
      this.ctx.moveTo(0, x);
      this.ctx.lineTo(500, x);
    }
    this.ctx.strokeStyle = "white";
    this.ctx.stroke();
    this.ctx.fillStyle = "white";
    this.ctx.lineWidth = 1;
    Koch.KochLine(this.canvas, this.ctx, this.level);
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = "black";
    this.ctx.font = "30px Knockout47";
    this.ctx.fillText("Número de generación = " + this.level, 100, 330);
    this.ctx.strokeText("Número de generación = " + this.level, 100, 330);
    this.ctx.fillText("Número de segmentos = " + this.segments[this.level], 100, 360);
    this.ctx.strokeText("Número de segmentos = " + this.segments[this.level], 100, 360);
    this.ctx.fillText("Tamaño del segmento = " + this.size[this.level], 100, 390);
    this.ctx.strokeText("Tamaño del segmento = " + this.size[this.level], 100, 390);
    this.ctx.fillText("Longitud total = " + this.large[this.level], 100, 420);
    this.ctx.strokeText("Longitud total = " + this.large[this.level], 100, 420);
  }
}