import * as Koch from './koch.js';

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.ctx = this.canvas.getContext("2d");

    this.right = this.canvas.width;
    this.bottom = this.canvas.height;
  }

  /**
   * 
   */
  processInput() {

  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {

  }

  /**
   * 
   */
  render() {
    this.ctx.fillStyle = "black";
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.lineWidth = 0.5;
    for (let x = 0.5; x < 500; x += 50) {
      this.ctx.moveTo(x, 0);
      this.ctx.lineTo(x, 500);
      this.ctx.moveTo(0, x);
      this.ctx.lineTo(500, x);
    }
    this.ctx.strokeStyle = "#ddd";
    this.ctx.stroke();
    Koch.KochLine(this.canvas, this.ctx, 5);
  }

}