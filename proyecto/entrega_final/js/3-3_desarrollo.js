$(document).ready(function () {
    var canvas = document.getElementById('gosper1');
    var ctx = canvas.getContext('2d');
    var img = new Image();
    var generation = 1;
    var attempt1 = 1;
    var attempt2 = 1;
    var attempt3 = 1;
    var modal2 = $("#myModal2");
    var modal3 = $("#myModal3");
    var modal4 = $("#myModal4");
    var modal2Text = modal2.find('.modal-text');
    var modal3Text = modal3.find('.modal-text');
    var modal4Text = modal4.find('.modal-text');
    var answer1 = ["3^n", "(3^n)", "3^(n)", "(3)^n", "(3)^(n)"];
    img.onload = function() {
        ctx.drawImage(img, 0, 0, 480, 480);
    };
    img.src = './../img/matchpoint/contorno1.png';
    $('#restart').click(function () {
        generation = 1;
        img.onload = function() {
            ctx.drawImage(img, 0, 0, 480, 480);
        };
        img.src = './../img/matchpoint/contorno1.png';
    });
    $('.close').click(function () {
        $(".modal").css("display", "none");
    });
    $('#check1').click(function () {
        var segmentsInput = $("#segmentsInput").val().replace(/\s/g, '');
        modal2.css("display", "");
        if(answer1.includes(segmentsInput)) {
            modal2Text.load("./scenes/3-2_desarrollo-9.html");
        } else {
            if (attempt1 == 3) {
                modal2Text.load("./scenes/3-3_desarrollo-2.html", function (response, status, xhr) {
                    if (status == "success") {
                        modal2Text.append('<button class="bouncy" id="answer1">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt1 = 0;
            } else {
                modal2Text.load("./scenes/3-3_desarrollo-2.html");
            }
            attempt1++;
        } 
    });
    $('#check2').click(function () {
        var formulaInput = $("#formulaInput").val().replace(/\s/g, '');
        modal3.css("display", "");
        if(formulaInput == "(1/sqrt(7))^n") {
            modal3Text.load("./scenes/3-2_desarrollo-9.html");
        } else {
            if (attempt2 == 3) {
                modal3Text.load("./scenes/3-3_desarrollo-3.html", function (response, status, xhr) {
                    if (status == "success") {
                        modal3Text.append('<button class="bouncy" id="answer2">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt2 = 0;
            } else {
                modal3Text.load("./scenes/3-3_desarrollo-3.html");
            }
            attempt2++;
        }
    });
    $('#check3').click(function () {
        var curveInput = $("#curveInput").val().replace(/\s/g, '');
        modal4.css("display", "");
        if(curveInput == "1.129") {
            modal4Text.load("./scenes/3-2_desarrollo-9.html");
        } else {
            if (attempt3 == 3) {
                modal4Text.load("./scenes/3-3_desarrollo-5.html", function (response, status, xhr) {
                    if (status == "success") {
                        modal4Text.append('<button class="bouncy" id="answer3">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt3 = 0;
            } else {
                modal4Text.load("./scenes/3-3_desarrollo-5.html");
                attempt3++;
            }
        }
    });
    $('#nextGeneration').click(function () {
        if(generation < 7) {
            generation++;
        }
        switch(generation) {
            case 2:
                img.src = './../img/matchpoint/contorno2.png';
                break;
            case 3:
                img.src = './../img/matchpoint/contorno3.png';
                break;
            case 4:
                img.src = './../img/matchpoint/contorno4.png';
                break;
            case 5:
                img.src = './../img/matchpoint/contorno5.png';
                break;
            case 6:
                img.src = './../img/matchpoint/contorno6.png';
                break;
            case 7:
                img.src = './../img/matchpoint/contorno7.png';
                break;
        }
    });
    // Get the modal
    var modal = document.getElementById("myModal");
    // Get the button that opens the modal
    var btn = document.getElementById("construction");
    // // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks on the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }
    // // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});
