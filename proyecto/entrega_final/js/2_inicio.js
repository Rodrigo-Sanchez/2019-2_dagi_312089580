$(document).ready(function () {
    let canvas = document.querySelector("#koch");
    let ctx = canvas.getContext("2d");
    let action = true;
    let zoom = 0
    $('#zoomIn').click(function () {
        if(zoom < 16) {
            ctx.translate(canvas.width / 4, canvas.width / 4);
            ctx.scale(1.1, 1.1);
            ctx.translate(-canvas.width / 4, -canvas.height / 4);
            if(action) {
                $('.section1').append('<button class="bouncy" id="nextSection">Siguiente</button>');
                $('#nextSection').on('click', function() {
                    $('.section1').load("./scenes/2_inicio-2.html");
                });
                action = false;
            }
            zoom++;
        }
    });
    $('#zoomOut').click(function () {
        if(zoom > 2) {
            ctx.translate(canvas.width / 4, canvas.width / 4);
            ctx.scale(0.9, 0.9);
            ctx.translate(-canvas.width / 4, -canvas.height / 4);
            if(action) {
                $('.section1').append('<button class="bouncy" id="nextSection">Siguiente</button>');
                $('#nextSection').on('click', function() {
                    $('.section1').load("./scenes/2_inicio-2.html");
                });
                action = false;
            }
            zoom--;
        }
    });
});