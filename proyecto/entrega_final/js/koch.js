var n;    		    // Nivel máximo.
var x1, x2;         // Punto inicial.
var lenght;		 	// Longitud de la línea.
var start = 0;      // Ángulo inicial, (radianes).	
var degree;			// Grado en radianes.

/**
 * 
 * @param {*} canvas 
 * @param {*} ctx 
 * @param {*} MaxLevel 
 */
export function KochLine(canvas, ctx, MaxLevel) {
    n = MaxLevel;
    x1 = 0;
    x2 = canvas.height/2;
    lenght = canvas.width;
    degree = Math.PI / 180;
    ctx.clearRect(0, 0, ctx.width, ctx.height);
    render(ctx, x1, x2, start, lenght, 0);
}

/**
 * 
 * @param {*} ctx 
 * @param {*} x1 
 * @param {*} y1 
 * @param {*} alpha 
 * @param {*} lenght 
 * @param {*} level 
 */
function render(ctx, x1, y1, alpha, lenght, level) {
    if (level < n) {
        var L = lenght / 3;
        render(ctx, x1, y1, alpha, L, level + 1);
        var x = x1 + L * Math.cos(alpha);
        var y = y1 - L * Math.sin(alpha);
        var a = alpha + 60 * degree;
        render(ctx, x, y, a, L, level + 1);
        x = x + L * Math.cos(a);
        y = y - L * Math.sin(a);
        a = alpha - 60 * degree;
        render(ctx, x, y, a, L, level + 1);
        x = x1 + 2 * L * Math.cos(alpha);
        y = y1 - 2 * L * Math.sin(alpha);
        render(ctx, x, y, alpha, L, level + 1);
    } else {
        // Draw a line
        var x2 = x1 + lenght * Math.cos(alpha);
        var y2 = y1 - lenght * Math.sin(alpha);
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
    }
}