import Interactive from "./interactive.js";

let lastTime = Date.now();
let current = 0;
let elapsed = 0;
let max_elapsed_wait = 30 / 1000;
let container = document.getElementById("container")

var interactive = new Interactive(container);

/**
 * 
 */
(function gameLoop() {
  current = Date.now();
  elapsed = (current - lastTime) / 1000;

  if (elapsed > max_elapsed_wait) {
    elapsed = max_elapsed_wait;
  }

  interactive.processInput();
  interactive.update(elapsed);
  interactive.render();

  lastTime = current;

  window.requestAnimationFrame(gameLoop);
})();
