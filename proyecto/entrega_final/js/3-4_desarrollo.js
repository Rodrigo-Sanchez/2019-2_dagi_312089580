$(document).ready(function () {
    var mosaic = false;
    var separate = false;
    var separateText = "";
    var canvas = document.getElementById('gosper2');
    var ctx = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
        ctx.drawImage(img, 0, 0, 480, 480);
    };
    img.src = './../img/setpoint/isla180.png';
    $('#mosaic').on('click', function () {
        if(mosaic) {
            img.src = './../img/setpoint/isla180.png';
        } else {
            img.src = './../img/setpoint/isla180_mosaico.png';
        }
        mosaic = !mosaic;
    });
    $('#separate').on('click', function () {
        separateText = separate ? 'Separar' : 'Juntar';
        $("#separate").html(separateText);
        if(separate) {
            img.src = './../img/setpoint/isla180.png';
        } else {
            img.src = './../img/setpoint/isla180_separado.png';
        }
        separate = !separate;
    });
    $('#nextSection1').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-2.html");
    });
    $('#nextSection2').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-3.html");
    });
    $('#previousSection2').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-1.html");
    });
    $('#nextSection3').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-4.html");
    });
    $('#previousSection3').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-2.html");
    });
    $('#previousSection4').click(function () {
        $('.section1').load("./scenes/3-4_desarrollo-3.html");
    });
    $('.close').click(function () {
        $(".modal").css("display", "none");
    });
});
