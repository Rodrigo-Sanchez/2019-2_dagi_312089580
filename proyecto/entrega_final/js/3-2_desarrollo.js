
$(document).ready(function () {
    var attempt1 = 1;
    var attempt2 = 1;
    var attempt3 = 1;
    var modal2 = $("#myModal2");
    var modal3 = $("#myModal3");
    var modal4 = $("#myModal4");
    var modal2Text = modal2.find('.modal-text');
    var modal3Text = modal3.find('.modal-text');
    var modal4Text = modal4.find('.modal-text');
    var canvas = document.getElementById('hilbert');
    var ctx = canvas.getContext('2d');
    var img = new Image();
    var generation = 1;
    var network = false;
    var networkText = "";
    var predecesor = false;
    var predecesorText = "";
    img.onload = function () {
        ctx.drawImage(img, 0, 0, 480, 480);
    };
    img.src = './../img/doublefault/hilbert1.png';
    $('#nextModal').click(function () {
        $('.modal-text').load("./scenes/3-2_desarrollo-1.html");
    });
    $('.close').click(function () {
        $(".modal").css("display", "none");
    });
    $('#check1').click(function () {
        var segmentsInput = $("#segmentsInput").val().replace(/\s/g, '');
        modal2.css("display", "");
        if (segmentsInput == "2^(2n)-1") {
            modal2Text.load("./scenes/3-2_desarrollo-3.html");
        } else {
            if (attempt1 == 3) {
                modal2Text.load("./scenes/3-2_desarrollo-2.html", function (response, status, xhr) {
                    if (status == "success") {
                        modal2Text.append('<button class="bouncy" id="answer1">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt1 = 0;
            } else {
                modal2Text.load("./scenes/3-2_desarrollo-2.html");
            }
            attempt1++;
        }
    });
    $('#check2').click(function () {
        var formulaInput = $("#formulaInput").val().replace(/\s/g, '');
        modal3.css("display", "");
        if (formulaInput == "1/2^n") {
            modal3Text.load("./scenes/3-2_desarrollo-9.html");
        } else {
            if (attempt2 == 3) {
                modal3Text.load("./scenes/3-2_desarrollo-4.html", function (responseTxt, status, xhr) {
                    if (status == "success") {
                        modal3Text.append('<button class="bouncy" id="answer2">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt2 = 0;
            } else {
                modal3Text.load("./scenes/3-2_desarrollo-4.html");
            }
            attempt2++;
        }
    });
    $('#check3').click(function () {
        var curveInput = $("#curveInput").val().replace(/\s/g, '');
        modal4.css("display", "");
        if (curveInput == "2") {
            modal4Text.load("./scenes/3-2_desarrollo-5.html");
        } else {
            if (attempt3 == 3) {
                modal4Text.load("./scenes/3-2_desarrollo-10.html", function (responseTxt, status, xhr) {
                    if (status == "success") {
                        modal4Text.append('<button class="bouncy" id="answer3">Ver respuesta</button>');
                    } else if (status == "error") {
                        console.log("Error: " + xhr.status + ": " + xhr.statusText);
                    }
                });
                attempt3 = 0;
            } else {
                modal4Text.load("./scenes/3-2_desarrollo-10.html");
                attempt3++;
            }
        }
    });
    $('#network').on('click', function () {
        networkText = network ? 'Mostar red' : 'Ocultar red';
        $("#network").html(networkText);
        switch (generation) {
            case 1:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert1.png';
                    predecesor = false;
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert1-red.png';
                    predecesor = false;
                } 
                break;
            case 2:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor-red.png';
                }
                break;
            case 3:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor-red.png';
                }
                break;
            case 4:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor-red.png';
                }
                break;
            case 5:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert5.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert5-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor-red.png';
                }
                break;
        }
        network = !network;
    });
    $('#nextGeneration').on('click', function () {
        if (generation < 6) {
            generation++;
        }
        switch (generation) {
            case 1:
                $("#nextGeneration").html("Avanzar");
                $("#predecesor").css("display", "none");
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert1.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert1-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1.png';
                }    
                break;
            case 2:
                $("#predecesor").css("display", "");
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2.png';
                }
                break;
            case 3:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3.png';
                }
                break;
            case 4:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4-red.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4.png';
                }
                break;
            case 5:
                $("#nextGeneration").html("Reiniciar");
                if (network) {
                    img.src = './../img/doublefault/hilbert5-red.png';
                } else if (predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert5.png';
                }
                generation = 0;
                break;
        }
    });
    $('#predecesor').on('click', function () {
        predecesorText = predecesor ? 'Mostar predecesor' : 'Ocultar predecesor';
        $("#predecesor").html(predecesorText);
        switch (generation) {
            case 1:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1-predecesor-red.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert1-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert1-predecesor.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert1.png';
                } 
                break;
            case 2:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor-red.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert2-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert2-predecesor.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert2.png';
                } 
                break;
            case 3:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor-red.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert3-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert3-predecesor.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert3.png';
                } 
                break;
            case 4:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor-red.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert4-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert4-predecesor.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert4.png';
                } 
                break;
            case 5:
                if (network && !predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor-red.png';
                } else if (network && predecesor) {
                    img.src = './../img/doublefault/hilbert5-red.png';
                } else if (!network && !predecesor) {
                    img.src = './../img/doublefault/hilbert5-predecesor.png';
                } else if (!network && predecesor) {
                    img.src = './../img/doublefault/hilbert5.png';
                } 
                break;
        }
        predecesor = !predecesor;
    });
    // When the user clicks anywhere outside of the modal, close it.
    window.onclick = function (evt) {
        var modal2 = document.getElementById("myModal2");
        var modal3 = document.getElementById("myModal3");
        var modal4 = document.getElementById("myModal4");
        switch (evt.target) {
            case modal2:
                modal2.style.display = "none";
                break;
            case modal3:
                modal3.style.display = "none";
                break;
            case modal4:
                modal4.style.display = "none";
                break;
        }
    }
});
