import Snake from "./Snake.js";

window.addEventListener("load", function(evt) {
  let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;
  let max_elapsed_wait = 30/1000;
  let counter_time = 0;

  let time_step = 0.05;
  let parts = 100;
  let counter = 0;

  let snake = new Snake();

  (function gameLoop() {
    current = Date.now();
    elapsed = (current - lastTime) / 1000;

    if (elapsed > max_elapsed_wait) {
      elapsed = max_elapsed_wait;
    }

    if (counter_time > time_step) {
      counter = (counter+1)%parts;
      counter_time = 0;
      snake.processInput();
      snake.update(elapsed);
      snake.render();
    }

    counter_time += elapsed;
    lastTime = current;

    window.requestAnimationFrame(gameLoop);
  })();
});