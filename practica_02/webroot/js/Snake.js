import KEY from "./Key.js";

const CELL = '1';
const SNAKE_HEAD = '2';
const SNAKE_BODY = '3';
const FOOD = '4';

const HEIGHT = 48;
const WIDTH = 64;

export default class Interactive {
    constructor() {
        this.container = document.querySelector("#container");
        this.counter = document.querySelector("#counter");
        this.retro = document.querySelector("#retro");
        this.retro_txt = document.querySelector("#retro_txt");
        this.retro_btn = document.querySelector("#retro_btn");

        this.game = true;

        this.score = 0;
        this.direction = KEY.UP;

        this.snakeHead = WIDTH * (HEIGHT / 2) + WIDTH / 2;
        this.snake = [this.snakeHead];
        this.snake.long = this.snake.length;

        this.matrix = Array.from(CELL.repeat(WIDTH * HEIGHT));

        this.food = this.generateFood();

        this.matrix[this.snakeHead] = SNAKE_HEAD;
        this.matrix[this.food] = FOOD;

        self = this;
        (function boardCreator() {
            self.matrix.forEach((cell, i) => {
                let div = document.createElement('div');
                div.id = "grid-" + i;
                div.classList.add(self.boardPaint(cell));
                container.appendChild(div);
                // TODO: Modificar el CSS para evitar imprimir el salto de línea, <br>
                if ((i + 1) % WIDTH == 0)
                    container.insertAdjacentHTML('beforeend', '<br>');
            });
        })();

        this.cells = [...this.container.querySelectorAll("div")];

        this.retro_btn.addEventListener("mousedown", () => this.restart());

        this.background_audio = new Audio("webroot/audio/snake_runner.mp3");
        this.background_audio.loop = true;
        this.background_audio.volume = 0.5;
        this.background_audio.addEventListener("canplay", function() {
            this.play();
        });

        this.eat_audio = new Audio("webroot/audio/level_up.mp3");
        this.eat_audio.volume = 0.5;

        this.lose_audio = new Audio("webroot/audio/game_over.mp3");
        this.lose_audio.volume = 1;

        window.addEventListener("keydown", (evt) => {
            KEY.onKeyDown(evt.key);
        });

        window.addEventListener("keyup", (evt) => {
            KEY.onKeyUp(evt.key);
        });
    }

    processInput() {
        if (this.game) {
            this.snakeEats();
            this.snakeCrashing();
            this.updateSnakeBody();
        }
    }

    update(elapsed) {
        if (this.game) {
            this.updateSnakeDirection();
            this.moveSnake();
            this.generateFood();
        }
    }

    render() {
        if (this.game) {
            this.updateBoard();
        }
    }

    /**
     * Función que actualiza la dirección en la cual se mueve la serpiente.
     */
    updateSnakeDirection() {
        if (KEY.isPressed(KEY.LEFT) && this.direction != KEY.RIGHT) {
            this.direction = KEY.LEFT;
        }
        if (KEY.isPressed(KEY.RIGHT) && this.direction != KEY.LEFT) {
            this.direction = KEY.RIGHT;
        }
        if (KEY.isPressed(KEY.UP) && this.direction != KEY.DOWN) {
            this.direction = KEY.UP;
        }
        if (KEY.isPressed(KEY.DOWN) && this.direction != KEY.UP) {
            this.direction = KEY.DOWN;
        }
    }

    /**
     * Función que mueve la cabeza de la serpiente en el tablero.
     */
    moveSnake() {
        switch (this.direction) {
            case KEY.LEFT:
                this.snakeHead--;
                this.snake.unshift(this.snakeHead);
                this.snake.pop();
                this.matrix[this.snakeHead] = SNAKE_HEAD;
                break;
            case KEY.RIGHT:
                this.snakeHead++;
                this.snake.unshift(this.snakeHead);
                this.snake.pop();
                this.matrix[this.snakeHead] = SNAKE_HEAD;
                break;
            case KEY.UP:
                this.snakeHead -= WIDTH;
                this.snake.unshift(this.snakeHead);
                this.snake.pop();
                this.matrix[this.snakeHead] = SNAKE_HEAD;
                break;
            case KEY.DOWN:
                this.snakeHead += WIDTH;
                this.snake.unshift(this.snakeHead);
                this.snake.pop();
                this.matrix[this.snakeHead] = SNAKE_HEAD;
                break;
        }
    }

    /**
     * Quita la cabeza anterior de la serpiente.
     */
    updateSnakeBody() {
        this.matrix.forEach((cell, i) => {
            if(this.matrix[i] === SNAKE_BODY || this.matrix[i] === SNAKE_HEAD) {
                this.matrix[i] = CELL;
            }
        });
        let cloned = JSON.parse(JSON.stringify(this.snake));
        cloned.pop();
        cloned.forEach((cell) => {
            this.matrix[cell] = SNAKE_BODY;
        });
    }

    /**
     * Funcion que pinta el tablero.
     * @param {String} cell la cadena correspondiente a la i-ésima entrada del arreglo.
     */
    boardPaint(cell) {
        switch (cell) {
            case CELL:
                return 'grid';
            case SNAKE_HEAD:
                return 'snakeHead';
            case SNAKE_BODY:
                return 'snakeBody';
            case FOOD:
                return 'food';
        }
    }

    /**
     * Función que actualiza el pintado del tablero.
     */
    updateBoard() {
        this.cells.forEach((cell, i) => {
            cell.className = this.boardPaint(this.matrix[i]);
        });
    }

    /**
     * Función que genera una posición segura para asignar la comida.
     */
    generateFood() {
        let position = Math.floor(Math.random() * (WIDTH * HEIGHT));
        if (!this.matrix.includes(FOOD)) {
            while (this.snake.includes(position)) {
                position = Math.floor(Math.random() * (WIDTH * HEIGHT));
            }
            this.food = position;
            this.matrix[this.food] = FOOD;
        }
        return position;
    }

    /**
     * Función que aumenta el contador de puntos cuando la serpiente come.
     */
    snakeEats() {
        switch (this.direction) {
            case KEY.LEFT:
                if (this.matrix[this.snakeHead - 1] === FOOD) {
                    this.counter.innerHTML = ++this.score;
                    this.snake.long++;
                    this.snake.unshift(this.snakeHead - 1);
                    this.eat_audio.pause();
                    this.eat_audio.play();
                }
                break;
            case KEY.RIGHT:
                if (this.matrix[this.snakeHead + 1] === FOOD) {
                    this.counter.innerHTML = ++this.score;
                    this.snake.long++;
                    this.snake.unshift(this.snakeHead + 1);
                    this.eat_audio.pause();
                    this.eat_audio.play();
                }
                break;
            case KEY.UP:
                if (this.matrix[this.snakeHead - WIDTH] === FOOD) {
                    this.counter.innerHTML = ++this.score;
                    this.snake.long++;
                    this.snake.unshift(this.snakeHead - WIDTH);
                    this.eat_audio.pause();
                    this.eat_audio.play();
                }
                break;
            case KEY.DOWN:
                if (this.matrix[this.snakeHead + WIDTH] === FOOD) {
                    this.counter.innerHTML = ++this.score;
                    this.snake.long++;
                    this.snake.unshift(this.snakeHead + WIDTH);
                    this.eat_audio.pause();
                    this.eat_audio.play();
                }
                break;
        }
    }

    /**
     * Función que detecta cuando la serpiente choca.
     */
    snakeCrashing() {
        if(this.direction === KEY.LEFT && ((this.snakeHead % 64) === 0 || this.matrix[this.snakeHead - 1] === SNAKE_BODY)
            || this.direction === KEY.RIGHT && ((this.snakeHead % 64) === 63 || this.matrix[this.snakeHead + 1] === SNAKE_BODY)
            || this.direction === KEY.UP && (this.snakeHead - WIDTH < 0 || this.matrix[this.snakeHead - WIDTH] === SNAKE_BODY)
            || this.direction === KEY.DOWN && (this.snakeHead + WIDTH > WIDTH * HEIGHT || this.matrix[this.snakeHead + WIDTH] === SNAKE_BODY)) {
            this.retro.style.display = 'block';
            this.lose_audio.play();
            this.background_audio.pause();
            this.game = false;
        }
    }

    /**
     * Función que reinicia el juego.
     */
    restart() {
        window.location.reload();
    }
}