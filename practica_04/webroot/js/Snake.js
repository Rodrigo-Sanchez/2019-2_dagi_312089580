import KEY from "./Key.js";

const CELL = '1';
const SNAKE_HEAD = '2';
const SNAKE_BODY = '3';
const FOOD = '4';

const HEIGHT = 48;
const WIDTH = 64;
const SQUARE = 4.743083;

export default class Interactive {
    constructor() {
        this.svg_namespace = "http://www.w3.org/2000/svg";
        this.container = document.querySelector("object");
        this.svg_document = this.container.getSVGDocument();
        this.svg = this.svg_document.querySelector("svg");
        this.counter = document.querySelector("#counter");
        this.retro = document.querySelector("#retro");
        this.retro_txt = document.querySelector("#retro_txt");
        this.retro_btn = document.querySelector("#retro_btn");

        this.game = true;

        this.score = 0;
        this.direction = KEY.UP;

        this.snakeHead = WIDTH * (HEIGHT / 2) + WIDTH / 2;
        this.snake = [this.snakeHead];
        this.snake.x = WIDTH / 2;
        this.snake.y = HEIGHT / 2;

        this.food = [this.generateFood(true), this.generateFood(false)];

        this.retro_btn.addEventListener("mousedown", () => this.restart());

        this.background_audio = new Audio("webroot/audio/snake_runner.mp3");
        this.background_audio.loop = true;
        this.background_audio.volume = 0.5;
        this.background_audio.addEventListener("canplay", function () {
            this.play();
        });

        this.eat_audio = new Audio("webroot/audio/level_up.mp3");
        this.eat_audio.volume = 0.5;

        this.lose_audio = new Audio("webroot/audio/game_over.mp3");
        this.lose_audio.volume = 1;

        window.addEventListener("keydown", (evt) => {
            KEY.onKeyDown(evt.key);
        });

        window.addEventListener("keyup", (evt) => {
            KEY.onKeyUp(evt.key);
        });
    }

    processInput() {
        if (this.game) {
            this.updateSnakeDirection();
        }
    }

    update(elapsed) {
        if (this.game) {
            this.snakeCrashing();
            this.snakeEats();
            this.moveSnake();
        }
    }

    render() {
        if (this.game) {
            this.updateBoard();
        }
    }

    /**
     * Función que actualiza la dirección en la cual se mueve la serpiente.
     */
    updateSnakeDirection() {
        if (KEY.isPressed(KEY.LEFT) && this.direction != KEY.RIGHT) {
            this.direction = KEY.LEFT;
        }
        if (KEY.isPressed(KEY.RIGHT) && this.direction != KEY.LEFT) {
            this.direction = KEY.RIGHT;
        }
        if (KEY.isPressed(KEY.UP) && this.direction != KEY.DOWN) {
            this.direction = KEY.UP;
        }
        if (KEY.isPressed(KEY.DOWN) && this.direction != KEY.UP) {
            this.direction = KEY.DOWN;
        }
    }

    /**
     * Función que mueve la cabeza de la serpiente en el tablero.
     */
    moveSnake() {
        switch (this.direction) {
            case KEY.LEFT:
                this.snake.x--;
                break;
            case KEY.RIGHT:
                this.snake.x++;
                break;
            case KEY.UP:
                this.snake.y--;
                break;
            case KEY.DOWN:
                this.snake.y++;
                break;
        }
        this.snake.unshift([this.snake.x, this.snake.y]);
        this.snake.pop();
    }

    /**
     * Función que actualiza el pintado del tablero.
     */
    updateBoard() {
        this.deleteCells();
        this.boardPaint(FOOD, this.food[0] * SQUARE, this.food[1] * SQUARE);
        this.boardPaint(SNAKE_HEAD, this.snake.x * SQUARE, this.snake.y * SQUARE);
        this.snakeBodyPaint();
    }

    /**
     * Función que borra las celdas que no son la base del tablero.
     */
    deleteCells() {
        let snakeHead = this.container.contentDocument.getElementsByClassName(2);
        for (let i = snakeHead.length - 1; i >= 0; --i) {
            snakeHead[i].remove();
        }
        let snakeBody = this.container.contentDocument.getElementsByClassName(3);
        for (let i = snakeBody.length - 1; i >= 0; --i) {
            snakeBody[i].remove();
        }
        let food = this.container.contentDocument.getElementsByClassName(4);
        for (let i = food.length - 1; i >= 0; --i) {
            food[i].remove();
        }
    }

    /**
     * Función que pinta el cuerpo de la serpiente-
     */
    snakeBodyPaint() {
        let snakeBody = JSON.parse(JSON.stringify(this.snake));
        snakeBody.shift();
        snakeBody.forEach((cell, i) => {
            this.boardPaint(SNAKE_BODY, snakeBody[i][0] * SQUARE, snakeBody[i][1] * SQUARE);
        });
    }

    /**
     * Función que pinta el tablero.
     * @param {Number} option La opción que corresponde al código que lo distingue en su dibujado.
     * @param {Number} x La coordenada en x donde vamos a dibujar. 
     * @param {Number} y La coordenada en y donde vamos a dibujar.
     */
    boardPaint(option, x, y) {
        let rect = document.createElementNS(this.svg_namespace, "rect");
        rect.setAttribute("x", x);
        rect.setAttribute("y", y);
        rect.setAttribute("width", SQUARE);
        rect.setAttribute("height", SQUARE);
        rect.setAttribute("stroke-width", 0.25);

        switch(option) {
            case SNAKE_HEAD:
                rect.setAttribute("fill", "#9bbc0f");
                rect.setAttribute("stroke", "#000000");
                rect.setAttribute("class", SNAKE_HEAD);
                break;
            case SNAKE_BODY:
                rect.setAttribute("fill", "#9bbc0f");
                rect.setAttribute("stroke", "#8bac0f");
                rect.setAttribute("class", SNAKE_BODY);
                break;
            case FOOD:
                rect.setAttribute("fill", "#e74c3c");
                rect.setAttribute("stroke", "#000000");
                rect.setAttribute("class", FOOD);
                break;
        }

        this.svg.appendChild(rect);
    }

    /**
     * Función que genera una posición segura para asignar la comida.
     */
    generateFood(axis) {
        let position = axis ? Math.floor(Math.random() * WIDTH) : Math.floor(Math.random() * HEIGHT);
        while (this.snake.includes(position)) {
            position = axis ? Math.floor(Math.random() * WIDTH) : Math.floor(Math.random() * HEIGHT);
        }
        return position;
    }

    /**
     * Función que aumenta el contador de puntos cuando la serpiente come.
     */
    snakeEats() {
        if (this.snake.x === this.food[0] && this.snake.y === this.food[1]) {
            this.counter.innerHTML = ++this.score;
            this.snake.unshift([this.snake.x, this.snake.y]);
            this.eat_audio.pause();
            this.eat_audio.currentTime = 0;
            this.eat_audio.play();
            this.food = [this.generateFood(true), this.generateFood(false)];
        }
    }

    /**
     * Función que detecta cuando la serpiente choca.
     */
    snakeCrashing() {
        if (this.direction === KEY.LEFT && this.snake.x === 0 ||
            this.direction === KEY.RIGHT && this.snake.x === WIDTH - 1 ||
            this.direction === KEY.UP && this.snake.y === 0 ||
            this.direction === KEY.DOWN && this.snake.y === HEIGHT - 1) {
            this.retro.style.display = 'block';
            this.lose_audio.play();
            this.background_audio.pause();
            this.game = false;
        }

        let snakeBody = JSON.parse(JSON.stringify(this.snake));
        snakeBody.shift();
        snakeBody.forEach((cell, i) => {
            if (snakeBody[i][0] === this.snake.x && snakeBody[i][1] === this.snake.y) {
                this.retro.style.display = 'block';
                this.lose_audio.play();
                this.background_audio.pause();
                this.game = false;
            }
        });
    }

    /**
     * Función que reinicia el juego.
     */
    restart() {
        window.location.reload();
    }
}