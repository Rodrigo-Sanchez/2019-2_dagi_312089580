# Nombre #
Sánchez Morales Rodrigo Alejandro

# Número de cuenta #
312089580

# E-mail #
rodrigosanchez@ciencias.unam.mx

## Descripción ##
Este repositorio fue creado para la asignatura de _Seminario de Ciencias de la Computación A, Desarrollo de Aplicaciones Gráficas Interactivas_ correspondiente al semestre 2019-II impartido como materia optativa del plan de estudios de la licenciatura en Ciencias de la Computación con sede en la Facultad de Ciencias de la Universidad Nacional Autónoma de México.