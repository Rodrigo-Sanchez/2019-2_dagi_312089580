import {Vec2D, Vec3D} from "./Vectores.js";

console.log(Vec2D.addStatic(new Vec3D(5, 6, 2), new Vec3D(1, 3, 7)));

let tmp_v = {};
tmp_v.x = 4;
tmp_v.y = 5;
tmp_v.z = 9;

console.log(Vec3D.addStatic(tmp_v, {z:-1, y:200, x:-3.1415, w:-200, saludo:"hola"}));