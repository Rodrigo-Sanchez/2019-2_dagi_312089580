export default class Ball {
  constructor(x=0, y=0, radius=1, color="#b71540") {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
    let angle = 45;
    this.speed = 150;
    this.velocity = {
      x: this.speed * Math.cos(degreeToRadian(angle)),
      y: this.speed * Math.sin(degreeToRadian(angle))
    };
  }

  update(elapsed) {
    this.x = this.x + this.velocity.x * elapsed;
    this.y = this.y + this.velocity.y * elapsed;
  }

  render(context) {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI*2);
    context.fillStyle = this.color;    
    context.fill();
  }

  setAngularVelocity(angle) {
    this.velocity.x = this.speed * Math.cos(angle);
    this.velocity.y = this.speed * Math.sin(angle);
  }
}

function degreeToRadian(d) {
  return d * Math.PI / 180;
}