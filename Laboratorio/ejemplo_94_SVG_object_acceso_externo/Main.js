window.addEventListener("load", function() {
  let svg_namespace = "http://www.w3.org/2000/svg";

  let container = document.querySelector("object");
  let svg_document = container.getSVGDocument();
  
  let svg = svg_document.querySelector("svg");

  let rect = document.createElementNS(svg_namespace, "rect");
  rect.setAttribute("x", 150);
  rect.setAttribute("y", 150);
  rect.setAttribute("width", 100);
  rect.setAttribute("height", 100);
  rect.setAttribute("fill", "#aacc33");
  rect.setAttribute("stroke", "#000000");
  rect.setAttribute("stroke-width", 3);

  svg.appendChild(rect);
});