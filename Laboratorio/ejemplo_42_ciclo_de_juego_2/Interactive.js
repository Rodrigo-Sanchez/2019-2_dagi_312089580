export default class Interactive {
  constructor(container) {
    this.container = container;

    this.block = document.getElementById("cuadro");
    this.block.r = 0;
    this.block.g = 128;
    this.block.b = Math.random()*256;
    this.block.speed = 15;
  }

  processInput() {
    
  }

  update(elapsed) {
    this.block.r = (this.block.r + this.block.speed * elapsed) % 256;
    this.block.g = (this.block.g + this.block.speed * elapsed) % 256;
    this.block.b = (this.block.b + this.block.speed * elapsed) % 256;

    this.block.style.backgroundColor = `rgb(${this.block.r},${this.block.g},${this.block.b})`;
  }

  render() {

  }
}
