if (true) {
  var var_x = 123;
}
console.log(var_x);

if (true) {
  let_x = 123;
}
console.log(let_x);

const const_x = 4;
if (true) {
  const_x = 123;
}
console.log(const_x);
