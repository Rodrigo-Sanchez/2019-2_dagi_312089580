window.addEventListener("load", function() {
  let svg_document = document.getElementById("interactive").getSVGDocument();
  let svg = svg_document.querySelector("svg");

  let text_group = svg.getElementById("text_group");
  text_group.setAttribute("style", "pointer-events: none;");

  let display_text = svg.getElementById("display_text");

  let btn_group = svg.getElementById("btn_group");
  btn_group.setAttribute("style", "cursor: pointer;");

  btn_group.querySelectorAll("rect").forEach(btn => {
    btn.setAttribute("class", "btn");
    let val = btn.getAttribute("id").substring(4);

    btn.addEventListener("click", () => {
      setValue(val);
    });
  });

  function setValue(val) {
    if (val === "=") {
      try {
        display_text.textContent = eval(display_text.textContent);
      }
      catch(err) {
        display_text.textContent = "ERROR";
      }
    }
    else {
      if ((display_text.textContent === "ERROR") || ((display_text.textContent === "0") && (val !== "."))) {
        display_text.textContent = val;
      }
      else {
        display_text.textContent += val;
      }
    }
  }

});