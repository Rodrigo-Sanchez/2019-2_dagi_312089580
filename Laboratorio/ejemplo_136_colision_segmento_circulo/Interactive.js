import Segment from "./Segment.js";
import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment = new Segment(this.canvas.width/3, this.canvas.height/2 -50, 2*this.canvas.width/3, this.canvas.height/2 +50, "#3498db");
    this.projection = {x:0, y:0, in_segment:false};

    this.circle = new Ball(this.mouse_position.x, this.mouse_position.y, 30);
  }

  processInput() {
  }

  update(elapsed) {
    this.circle.x = this.mouse_position.x;
    this.circle.y = this.mouse_position.y;

    this.collisionSegmentCircle(this.segment, this.circle);
  }

  collisionSegmentCircle(segment, circle) {
    if (
      this.collisionCirclePoint(circle, { x:segment.x1, y:segment.y1 }) || 
      this.collisionCirclePoint(circle, { x:segment.x2, y:segment.y2 })
    ) {
      this.segment.color = "#e67e22";
      return;
    }

    this.get_projection(circle.x, circle.y);

    if (this.collisionCirclePoint(circle, this.projection) && this.projection.in_segment) {
      this.segment.color = "#e67e22";
      return;
    }

    this.segment.color = "#3498db";
  }

  collisionCirclePoint(circle, point) {
    let dist = distance(circle.x, circle.y, point.x, point.y);

    if (dist < circle.radius) {
      return true;
    }
    else {
      return false;
    }
  }

  get_projection(x, y) {
    let segment_length = distance(this.segment.x1, this.segment.y1, this.segment.x2, this.segment.y2);

    let segment_unit_vector_x = (this.segment.x2 - this.segment.x1)/segment_length;
    let segment_unit_vector_y = (this.segment.y2 - this.segment.y1)/segment_length;

    let dot = dotProduct(segment_unit_vector_x, segment_unit_vector_y, x - this.segment.x1, y - this.segment.y1);

    this.projection.x = this.segment.x1 + dot * segment_unit_vector_x;
    this.projection.y = this.segment.y1 + dot * segment_unit_vector_y;

    if ((dot >= 0) && (dot <= segment_length)) {
      this.projection.in_segment = true;
    }
    else {
      this.projection.in_segment = false;
    }

    return this.projection;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.context.setLineDash([]);

    this.segment.render(this.context);
    this.circle.render(this.context);

    if (this.projection.in_segment) {
      this.context.beginPath();
      this.context.strokeStyle = "#000000";
      this.context.lineWidth = 1;
      this.context.setLineDash([6, 4]);
      this.context.moveTo(this.circle.x, this.circle.y);
      this.context.lineTo(this.projection.x, this.projection.y);
      this.context.stroke();

      this.context.beginPath();
      this.context.fillStyle = "orange";
      this.context.arc(this.projection.x, this.projection.y, 5, 0, Math.PI*2);
      this.context.fill();
    }
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}

function dotProduct(x1, y1, x2, y2) {
  return x1*x2 + y1*y2;
}