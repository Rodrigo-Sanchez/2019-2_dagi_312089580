let arreglo_1 = [];
console.log(arreglo_1);

let arreglo_2 = ["hola", 2, true, {x:0, y:0}, 'adios'];
console.log(arreglo_2);


let arreglo_3 = new Array();
console.log(arreglo_3);

let arreglo_4 = new Array(15);
console.log(arreglo_4);

let arreglo_5 = new Array("hola");
console.log(arreglo_5);

let arreglo_6 = new Array("hola", 2, true, {x:0, y:0}, 'adios');
console.log(arreglo_6);

console.log("el tamaño de arreglo_6 es", arreglo_6.length);



let str_1 = "una cadena";
console.log(str_1);

let str_2 = 'otra cadena';
console.log(str_2);

let str_3 = `también una cadena`;
console.log(str_3);

let str_4 = "primera 'parte' " + 'segunda "parte"';
console.log(str_4);

let str_5 = `primera 'parte' segunda "parte"`;
console.log(str_5);

let valor = 123;
let str_6 = "el valor es " + valor;
console.log(str_6);

let str_7 = `el valor es ${valor}`;
console.log(str_7);

console.log(`el tamaño de 
str7 es ${str_7.length}`);
