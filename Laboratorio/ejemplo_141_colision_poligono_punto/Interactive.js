import Polygon from "./Polygon.js";
import Segment from "./Segment.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment = new Segment(0, 0, 0, 0, 1, "#8e44ad");

    this.polygon = new Polygon([
      {x: 100, y: 100},
      {x: 350, y: 80},
      {x: 700, y: 140},
      {x: 500, y: 500},
      {x: 350, y: 200},
      {x: 130, y: 300},
    ], "#2ecc71");
  }

  processInput() {
  }

  update(elapsed) {
    this.collisionPolygonPoint(this.polygon, this.mouse_position);
  }

  collisionPolygonPoint(polygon, point) {
    this.segment.y1 = point.y;
    this.segment.x2 = point.x;
    this.segment.y2 = point.y;

    let intersection_counter = 0;
    let temp_intersection;
    let l = polygon.points.length;
    
    for (let i=0; i<l; i++) {
      temp_intersection = this.collisionSegmentSegment(this.segment, {
        x1: polygon.points[i].x,
        y1: polygon.points[i].y,
        x2: polygon.points[(i+1)%l].x,
        y2: polygon.points[(i+1)%l].y,
      });
      if (temp_intersection !== null) {
        intersection_counter++;
      }
    }

    if (intersection_counter%2 != 0) {
      polygon.color = "#2c3e50";
    }
    else {
      polygon.color = "#2ecc71";
    }
  }

  collisionSegmentSegment(segment_1, segment_2) {
    // pa = p1 + ua * (p2 - p1)
    // pb = p3 + ub * (p4 - p3)
    let x1 = segment_1.x1;
    let y1 = segment_1.y1;
    let x2 = segment_1.x2;
    let y2 = segment_1.y2;

    let x3 = segment_2.x1;
    let y3 = segment_2.y1;
    let x4 = segment_2.x2;
    let y4 = segment_2.y2;

    let ua = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
    let ub = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

    if ( (ua >= 0) && (ua <= 1) && (ub >= 0) && (ub <= 1) ) {
      return {
        x: x1 + (ua * (x2 - x1)),
        y: y1 + (ua * (y2 - y1))
      }
    }
    return null;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.polygon.render(this.context);
    this.segment.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

