export default class View {
  constructor() { }

  init(controller) {
    this.controller = controller;

    let svg_document = document.getElementById("toolbar").getSVGDocument()
    let toolbar = svg_document.querySelector("svg");

    toolbar.getElementById("btn_open").addEventListener("click", (evt) => {
      this.controller.open();
    });

    toolbar.getElementById("btn_save").addEventListener("click", (evt) => {
      this.controller.save();
    });

    let btn_line = toolbar.getElementById("btn_line");
    btn_line.querySelector("rect").style.strokeWidth = 5;

    btn_line.addEventListener("click", (evt) => {
      btn_line.querySelector("rect").style.stroke = "#ffff00";
      this.controller.line_mode();
    });

    this.canvas = document.getElementById("drawing");

    this.work_area = document.getElementById("work_area");

    this.aux_canvas = document.createElement("canvas");
    this.aux_canvas.addEventListener("mousedown", (evt) => {
      controller.mouse_down(evt, this.aux_canvas);
      window.addEventListener("mousemove", mouse_move);
      window.addEventListener("mouseup", mouse_up);
    });
    this.work_area.appendChild(this.aux_canvas);

    this.aux_context = this.aux_canvas.getContext("2d");

    function mouse_move(evt) {
      controller.mouse_move(evt)
    }

    let mouse_up = (evt) => {
      controller.mouse_up(this.aux_canvas);
      window.removeEventListener("mousemove", mouse_move);
      window.removeEventListener("mouseup", mouse_up);
    }
  }

  setTitle(tit) {
    let title = document.querySelector("title");
    if (!title) {
      title = document.createElement("title");
      document.head.appendChild(title);
    }

    title.textContent = tit;
  }

  changeSize(w, h) {
    this.canvas.setAttribute("width", w);
    this.canvas.setAttribute("height", h);
    this.aux_canvas.setAttribute("width", w);
    this.aux_canvas.setAttribute("height", h);

    this.canvas.style.display = this.aux_canvas.style.display = "block";
    this.canvas.style.left = this.aux_canvas.style.left = (this.work_area.offsetWidth  - w)/2 + "px";
    this.canvas.style.top  = this.aux_canvas.style.top  = (this.work_area.offsetHeight - h)/2 + "px";
  }

  drawLine(x1, y1, x2, y2) {
    this.clear();
    let ctx = this.aux_context;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
  }

  clear() {
    this.aux_context.clearRect(0, 0, this.aux_canvas.width, this.aux_canvas.height);
  }
}