import Bar from "./Bar.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.bar_1 = new Bar(0, 0, 150, 20, "#cc0000");
    this.bar_2 = new Bar(this.canvas.width/2, this.canvas.height/2, 150, 20, "rgba(142, 68, 173, 0.75)");
  }

  processInput() {
  }

  update(elapsed) {
    let target = this.reach(this.bar_1, this.mouse_position.x, this.mouse_position.y);

    this.reach(this.bar_2, target.x, target.y);
    
    this.position(this.bar_1, this.bar_2);
  }

  reach(bar, xpos, ypos) {
    let dx = xpos - bar.x;
    let dy = ypos - bar.y;

    bar.rotation = Math.atan2(dy, dx);

    let w = bar.getPin().x - bar.x;
    let h = bar.getPin().y - bar.y;

    return {
      x: xpos - w,
      y: ypos - h
    }
  }

  position(bar_1, bar_2) {
    bar_1.x = bar_2.getPin().x;
    bar_1.y = bar_2.getPin().y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.bar_1.render(this.context);
    this.bar_2.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
