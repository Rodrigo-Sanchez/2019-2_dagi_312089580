export default class Bar {
  constructor(x=0, y=0, w=1, h=1, color="#b71540") {
    this.x = x;
    this.y = y;

    this.rotation = 0;
    this.w = w;
    this.h = h;
    this.color = color;
  }

  update(elapsed) {
  }

  render(context) {
    let h = this.h;
    let d = this.w + h;
    let cr = h / 2;
    
    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.rotation);
    
    context.lineWidth = 1;
    context.fillStyle = this.color;

    context.beginPath();
    context.moveTo(0, -cr);
    context.lineTo(d-2*cr, -cr);
    context.quadraticCurveTo(-cr+d, -cr, -cr+d, 0);
    context.lineTo(-cr+d, h-2*cr);
    context.quadraticCurveTo(-cr+d, -cr+h, d-2*cr, -cr+h);
    context.lineTo(0, -cr+h);
    context.quadraticCurveTo(-cr, -cr+h, -cr, h-2*cr);
    context.lineTo(-cr, 0);
    context.quadraticCurveTo(-cr, -cr, 0, -cr);
    context.closePath();

    context.fill();
    context.stroke();

    context.beginPath();
    context.arc(0, 0, 2, 0, (Math.PI * 2), true);
    context.closePath();
    context.stroke();
    context.beginPath();
    context.arc(this.w, 0, 2, 0, (Math.PI * 2), true);
    context.closePath();
    context.stroke();
    context.restore();
  }

  getPin() {
    return {
      x: this.x + this.w * Math.cos(this.rotation),
      y: this.y + this.w * Math.sin(this.rotation)
    }
  }
}
