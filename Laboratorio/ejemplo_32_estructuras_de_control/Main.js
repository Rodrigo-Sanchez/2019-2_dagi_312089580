let i;
let n = 10;
let arr = ["a", "b", "c", "d"];
let obj = {x:1, y:-1};
let map = new Map([["key1", 1], ["key2", 2], ["key3", 3]]);
let str = "beeblebrox";

console.log("---- if ----");
/** if */
if (n < 100) {
  console.log("n es menor que 100");
}

console.log("---- if-else ----");
/** if-else */
if (n < 10) {
  console.log("n es menor que 10");
}
else {
  console.log("n es mayor o igual que 10");
}

console.log("---- if-else-2 ----");
/** if-else 2 */
if (obj) {
  console.log("el valor es DIFERENTE a: '', 0, false, undefined, null");
}
else {
  console.log("el valor es IGUAL a: '', 0, false, undefined, null");
}

console.log("---- if-else if-else ----");
/** if-else if-else */
if (n < 10)
  console.log("n es menor que 10");
else if (n === 10)
  console.log("n es igual que 10");
else 
  console.log("n es mayor que 10");

console.log("---- ()?: ----");
/** if ternario */
console.log( (n < 10) ? "n es menor que 10" : "n no es menor que 10" );

console.log("---- switch ----");
/** switch */
switch(n) {
  case 1: 
    console.log("el valor es 1");
    break;
  case 10:
    console.log("el valor es 10");
    break;
  default:
    console.log("ninguno de los anteriores");
    break;
}

console.log("---- while ----");
/** while */
i = 0
while(i < 3) {
  console.log("while: el valor de i es ", i++);
};

console.log("---- do-while ----");
/** do-while */
i = 0;
do {
  console.log("do-while: el valor de i es ", i++)
} while(i < 3);


console.log("---- for ----");
/** for */
for (i=0; i<3; i++) {
  console.log("for: el valor de i es ", i)
}

console.log("---- for-in ----");
/** for-in */
for (let v in obj) {
  console.log("obj[", v, "] =", obj[v]);
}

for (let v in map) {
  console.log("map.get(", v, ") =", map.get(v));
}

arr.iniciado = true;
for (let v in arr) {
  console.log("arr[", v, "] =", arr[v]);
}

for (let v in str) {
  console.log("str[", v, "] =", str[v]);
}

console.log("---- for-of ----");
/** for-of */
for (let v of map) {
  console.log(v);
}

for (let v of arr) {
  console.log(v);
}

for (let v of str) {
  console.log(v);
}


try {
  f(3);  
}
catch(err) {
  console.log(err);
}
console.log("hola despues del try-catch");