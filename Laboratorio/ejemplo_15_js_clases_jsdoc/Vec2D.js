/**
 * Clase que representa un vector en dos dimensiones
 */
export default class Vec2D {
  /**
   * Constructor de vectores de dos dimensiones
   * @param {Number} x - la coordenada en x
   * @param {Number} y - la coordenada en y
   */
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  
  /**
   * Suma dos vectores
   * @this {Vec2D} el vector desde donde se llama la función (el primer operador de la suma)
   * @param {Vec2D} v - el segundo vector con el que se realiza la suma
   * @return {Vec2D} Regresa la suma entre this y el vector v
   */
  add(v) {
    return new Vec2D(this.x + v.x, this.y + v.y);
  }
  
  /**
   * Suma estática entre dos vectores
   * @param {Vec2D} u - el primer vector a sumar
   * @param {Vec2D} v - el segundo vector a sumar
   * @return {Vec2D} Regresa la suma del vector u con el v
   */
  static addStatic(u, v) {
    return new Vec2D(u.x + v.x, u.y + v.y);
  }
}
