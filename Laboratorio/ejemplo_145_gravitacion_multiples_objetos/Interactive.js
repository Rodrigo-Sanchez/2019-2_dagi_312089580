import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    let left = 0;
    let right = this.canvas.width;
    let top = 0;
    let bottom = this.canvas.height;

    let PI_2 = 2*Math.PI;
    let num_balls = 50;
    let x;
    let y;
    let radius;
    let color;
    this.balls = [];

    this.balls[0] = new Ball(right/2, bottom/2, 10, 2000, "#000000");
    this.balls[0].velocity = {x: 0, y:0};

    for (let i=1; i<num_balls; i++) {
      radius = 10 + Math.random() * 25;
      color = `rgba(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}, 1)`;
      x = radius + Math.random()*(right - 2*radius);
      y = radius + Math.random()*(bottom - 2*radius);
      this.balls[i] = new Ball(x, y, radius, radius, color);
      this.balls[i].velocity = {x: 0, y: 0 };
    }
  }

  processInput() {
  }

  update(elapsed) {
    this.balls.forEach((ball, index) => {
      if (index > 0) {
        this.move(ball, elapsed);
      }
      for (let i=index+1; i<this.balls.length; i++) {
        this.gravitate(ball, this.balls[i]);
      }
    });
  }

  move(particle, elapsed) {
    particle.x += particle.velocity.x * elapsed;
    particle.y += particle.velocity.y * elapsed;
  }

  gravitate(particle_1, particle_2) {
    let dx = particle_2.x - particle_1.x;
    let dy = particle_2.y - particle_1.y;
    let distSQ = dx * dx + dy * dy;
    let force = particle_1.mass * particle_2.mass / distSQ;
    let angle = Math.atan2(dy, dx);
    let ax = force * Math.cos(angle);
    let ay = force * Math.sin(angle);

    particle_1.velocity.x += ax / particle_1.mass;
    particle_1.velocity.y += ay / particle_1.mass;
    particle_2.velocity.x -= ax / particle_2.mass;
    particle_2.velocity.y -= ay / particle_2.mass;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.balls.forEach((ball) => {
      ball.render(this.context);
    });
  }
}
