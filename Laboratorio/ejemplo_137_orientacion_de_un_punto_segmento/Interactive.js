import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.point_1 = new Ball(this.canvas.width/3, this.canvas.height/2 +50, 5, "#c0392b");
    this.point_2 = new Ball(2*this.canvas.width/3, this.canvas.height/2 -50, 5, "#2ecc71");
    this.point_3 = new Ball(this.mouse_position.x, this.mouse_position.y, 5, "#3498db");

    this.orientation = "colineal";
  }

  processInput() {
  }

  update(elapsed) {
    this.point_3.x = this.mouse_position.x;
    this.point_3.y = this.mouse_position.y;

    this.orientation = this.getSide(this.point_1, this.point_2, this.point_3);
  }

  getSide(p1, p2, p3) {
    let or = (p2.y - p1.y) * (p3.x - p2.x) - (p2.x - p1.x) * (p3.y - p2.y);

    if (or > 0) {
      return "izquierda";
    }
    else if (or < 0) {
      return "derecha";
    }
    else {
      return "colineal";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.context.fillStyle = "#000000";
    this.context.font = '28px serif';
    this.context.fillText(this.orientation, 20, 30);

    this.context.beginPath();
    this.context.strokeStyle = "#000000";
    this.context.lineWidth = 1;
    this.context.setLineDash([6, 4]);
    this.context.moveTo(this.point_1.x, this.point_1.y);
    this.context.lineTo(this.point_2.x, this.point_2.y);
    this.context.lineTo(this.point_3.x, this.point_3.y);
    this.context.stroke();    

    this.point_1.render(this.context);
    this.point_2.render(this.context);
    this.point_3.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

