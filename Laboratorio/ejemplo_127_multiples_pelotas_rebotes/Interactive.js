import Ball from "./Ball.js";

let left;
let right;
let top;
let bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    left = 0;
    right = this.canvas.width;
    top = 0;
    bottom = this.canvas.height;

    let num_balls = 100;
    let x;
    let y;
    let radius;
    let color;
    this.balls = [];

    for (let i=0; i<num_balls; i++) {
      radius = 10 + Math.random() * 25;
      color = `rgba(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}, 1)`;
      x = radius + Math.random()*(right - 2*radius);
      y = radius + Math.random()*(bottom - 2*radius);
      this.balls.push( new Ball(x, y, radius, color) );
    }
  }

  processInput() {
  }

  update(elapsed) {
    this.balls.forEach((ball) => {
      ball.update(elapsed);
      this.checkBoundaries(ball);
    });
  }

  checkBoundaries(ball) {
    if (ball.x + ball.radius > right) {
      ball.velocity.x *= -1;
      ball.x = right - ball.radius;
    }
    else if (ball.x - ball.radius < left) {
      ball.velocity.x *= -1;
      ball.x = left + ball.radius;
    }

    if (ball.y + ball.radius > bottom) {
      ball.velocity.y *= -1;
      ball.y = bottom - ball.radius;
    }
    else if (ball.y - ball.radius < top) {
      ball.velocity.y *= -1;
      ball.y = top + ball.radius;
    }
  }

  render() {
    this.context.fillStyle = "#ecf0f1";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.balls.forEach((ball) => {
      ball.render(this.context);
    });

  }
}