import Polygon from "./Polygon.js";
import Rectangle from "./Rectangle.js";
import Point from "./Point.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    let point_size = 5;
    this.points = [
      new Point(100, 100, point_size),
      new Point(350, 80,  point_size),
      new Point(700, 140, point_size),
      new Point(500, 500, point_size),
      new Point(350, 200, point_size),
      new Point(130, 300, point_size)
    ];

    this.polygon = new Polygon(this.points, "#2ecc71");

    this.bbox = new Rectangle(0, 0, 0, 0);
  }

  processInput() {
  }

  update(elapsed) {
    this.getBoundingBox();
  }

  getBoundingBox() {
    let min_x = Infinity;
    let max_x = -Infinity;
    let min_y = Infinity;
    let max_y = -Infinity;

    this.points.forEach((point) => {
      min_x = Math.min(point.x, min_x);
      max_x = Math.max(point.x, max_x);
      min_y = Math.min(point.y, min_y);
      max_y = Math.max(point.y, max_y);
    });

    this.bbox.x = min_x;
    this.bbox.y = min_y;
    this.bbox.w = max_x - min_x;
    this.bbox.h = max_y - min_y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.polygon.render(this.context);
    this.bbox.render(this.context);

    this.points.forEach((point) => {
      point.render(this.context);
    });
  }
}
