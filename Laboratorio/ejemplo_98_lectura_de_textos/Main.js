window.addEventListener("load", function(evt) {
  let file_input = document.getElementById("input");
  file_input.value = "";
  
  file_input.addEventListener("change", function(evt) {
    let file = this.files[0];
    console.log(file);

    const reader = new FileReader();
    reader.addEventListener("load", function(elem) {
      console.log(elem);
      console.log(reader.result);

      let div = document.createElement("div");
      div.textContent = reader.result;
      document.body.appendChild(div);
    });
    if (file) {
      reader.readAsText(file);
    }
  });
});