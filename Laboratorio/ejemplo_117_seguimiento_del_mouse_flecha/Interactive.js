import Arrow from "./Arrow.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.Arrow = new Arrow(this.canvas.width/2, this.canvas.height/2);

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });
  }

  processInput() {
  }

  update(elapsed) {
    let dx = this.mouse_position.x - this.Arrow.x;
    let dy = this.mouse_position.y - this.Arrow.y;
    let angle = Math.atan2(dy, dx);
    
    this.Arrow.setAngularVelocity(angle);    
    this.Arrow.update(elapsed);
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.Arrow.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}