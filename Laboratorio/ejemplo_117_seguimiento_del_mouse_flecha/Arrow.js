export default class Arrow {
  constructor(x=0, y=0, size=1, color="#b71540") {
    this.x = x;
    this.y = y;
    this.size = size;
    this.color = color;

    this.rotation = 0;

    let angle = 45;
    this.speed = 150;
    this.velocity = {
      x: this.speed * Math.cos(degreeToRadian(angle)),
      y: this.speed * Math.sin(degreeToRadian(angle))
    };
  }

  update(elapsed) {
    this.x = this.x + this.velocity.x * elapsed;
    this.y = this.y + this.velocity.y * elapsed;
  }

  render(context) {
    context.save();

    context.translate(this.x, this.y);
    context.rotate(this.rotation);
    context.scale(this.size, this.size);

    context.beginPath();
    context.moveTo( 20,   0);
    context.lineTo( 0,   -20);
    context.lineTo( 0,   -8);
    context.lineTo(-20,  -8);
    context.lineTo(-20,   8);
    context.lineTo( 0,    8);
    context.lineTo( 0,    20);
    context.closePath();
    
    context.fillStyle = this.color;    
    context.fill();
    
    context.restore();
  }

  setAngularVelocity(angle) {
    this.rotation = angle;
    this.velocity.x = this.speed * Math.cos(angle);
    this.velocity.y = this.speed * Math.sin(angle);
  }
}

function degreeToRadian(d) {
  return d * Math.PI / 180;
}