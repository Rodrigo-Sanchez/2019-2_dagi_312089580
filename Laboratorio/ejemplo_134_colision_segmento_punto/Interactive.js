import Segment from "./Segment.js";

const epsilon = 0.1;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment = new Segment(this.canvas.width/3, this.canvas.height/2 -50, 2*this.canvas.width/3, this.canvas.height/2 +50, "#3498db");
  }

  processInput() {
  }

  update(elapsed) {
    this.collisionSegmentPoint(this.segment, this.mouse_position);
  }

  collisionSegmentPoint(segment, point) {
    let dist_1 = distance(segment.x1, segment.y1, point.x, point.y);
    let dist_2 = distance(segment.x2, segment.y2, point.x, point.y);
    let dist_segment = distance(segment.x1, segment.y1, segment.x2, segment.y2);

    if (Math.abs(dist_1 + dist_2 - dist_segment) < epsilon) {
      this.segment.color = "#e67e22";
    }
    else {
      this.segment.color = "#3498db";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.segment.render(this.context);

    this.context.beginPath();
    this.context.fillStyle = "#000000";
    this.context.arc(this.mouse_position.x, this.mouse_position.y, 5, 0, Math.PI*2);
    this.context.fill();
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}