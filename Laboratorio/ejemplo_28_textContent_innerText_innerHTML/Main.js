window.addEventListener("load", function(evt) {
  let container = document.querySelector("#container");

  console.log("-----------------------textContent-----------------------")
  console.log(container.textContent);
  console.log("------------------------innerText------------------------")
  console.log(container.innerText);
  console.log("------------------------innerHTML------------------------")
  console.log(container.innerHTML);
  console.log("------------------------outerHTML------------------------")
  console.log(container.outerHTML);
  console.log("---------------------------------------------------------")
});