import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.Ball = new Ball(this.canvas.width/2, this.canvas.height/2, 20);
  }

  processInput() {
  }

  update(elapsed) {
    this.Ball.update(elapsed);
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.Ball.render(this.context);
  }
}