export default class Ball {
  constructor(x=0, y=0, radius=1, color="#b71540") {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
    this.angle = 45;
    this.speed = 50;
    this.velocity = {
      x: this.speed * Math.cos(degreeToRadian(this.angle)),
      y: this.speed * Math.sin(degreeToRadian(this.angle))
    };
  }

  update(elapsed) {
    this.x = this.x + this.velocity.x * elapsed;
    this.y = this.y + this.velocity.y * elapsed;
  }

  render(context) {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI*2);
    context.fillStyle = this.color;    
    context.fill();
  }
}

function degreeToRadian(d) {
  return d * Math.PI / 180;
}