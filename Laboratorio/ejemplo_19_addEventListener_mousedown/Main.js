window.addEventListener("load", function(evt) {
  let container = document.querySelector("#container");

  container.addEventListener("mousedown", function(evt) {
    console.log("clic en el contenedor");
  });

  let children = container.querySelectorAll(".special_div");
  for (let i=0; i<children.length; i++) {
    children[i].addEventListener("mousedown", function(evt) {
      console.log("clic en el hijo");
    });
  }
});