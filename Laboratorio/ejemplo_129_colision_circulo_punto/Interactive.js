import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.circle = new Ball(this.canvas.width/2, this.canvas.height/2, 100, "#3498db");
  }

  processInput() {
  }

  update(elapsed) {
    this.collisionCirclePoint(this.circle, this.mouse_position);
  }

  collisionCirclePoint(circle, point) {
    let dist = distance(circle.x, circle.y, point.x, point.y);

    if (dist < circle.radius) {
      this.circle.color = "#e67e22";
    }
    else {
      this.circle.color = "#3498db";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.circle.render(this.context);

    this.context.beginPath();
    this.context.strokeStyle = "#000000";
    this.context.lineWidth = 1;
    this.context.setLineDash([6, 4]);
    this.context.moveTo(this.circle.x, this.circle.y);
    this.context.lineTo(this.mouse_position.x, this.mouse_position.y);
    this.context.stroke();

    
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}