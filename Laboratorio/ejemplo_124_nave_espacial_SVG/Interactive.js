import Ship from "./Ship.js";
import KEY from "./Key.js";

let svg_namespace = "http://www.w3.org/2000/svg";
let left;
let right;
let top;
let bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.svg = container.querySelector("svg");
    let w = parseInt(this.svg.getAttribute("width"));
    let h = parseInt(this.svg.getAttribute("height"));

    this.background = this.svg.appendChild(document.createElementNS(svg_namespace, "rect"));
    this.background.setAttribute("x", 0);
    this.background.setAttribute("y", 0);
    this.background.setAttribute("width", w);
    this.background.setAttribute("height", h);
    this.background.setAttribute("fill", "#2c3e50")

    this.Ship = new Ship(this.svg, w/2, h/2, 40);

    left = 0;
    right = w;
    top = 0;
    bottom = h;

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });
    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  processInput() {
    this.Ship.processInput(KEY);
  }

  update(elapsed) {
    this.Ship.update(elapsed);

    this.checkBoundaries();
  }

  checkBoundaries() {
    if (this.Ship.x > right) {
      this.Ship.x = left;
    }
    else if (this.Ship.x < left) {
      this.Ship.x = right;
    }

    if (this.Ship.y > bottom) {
      this.Ship.y = top;
    }
    else if (this.Ship.y < top) {
      this.Ship.y = bottom;
    }
  }

  render() {
    this.Ship.render();
  }
}