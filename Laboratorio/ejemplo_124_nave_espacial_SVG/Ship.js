const PI_180 = Math.PI/180;
const drawing_angle = 125;
const ROTATE_ANGLE = 3;
const SHIP_FORCE = 20;
let cos;
let sin;
let size_2;
let size_3;
let size_8;
let svg_namespace = "http://www.w3.org/2000/svg";

export default class Ship {
  constructor(svg, x=0, y=0, size=1) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.vr = 0;
    this.rotation = 0;
    this.force = 0;
    this.friction = 0.95;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
    this.showFlame = false;

    this.graphics = svg.appendChild( document.createElementNS(svg_namespace, "g") );
    this.flame_graphic = this.graphics.appendChild( document.createElementNS(svg_namespace, "path") );
    this.ship_graphic = this.graphics.appendChild( document.createElementNS(svg_namespace, "path") );

    this.build_graphics();
  }

  /** */
  processInput(KEY) {
    if (KEY.isPressed(KEY.LEFT)) {
      this.rotateLeft();
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.rotateRight();
    }
    if (KEY.isPressed(KEY.UP)) {
      this.increaseSpeed();
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.reverseSpeed();
    }

    if (!KEY.isPressed(KEY.LEFT) && !KEY.isPressed(KEY.RIGHT)) {
      this.zeroRotation();
    }
    if ((!KEY.isPressed(KEY.UP)) && !KEY.isPressed(KEY.DOWN)) {
      this.zeroVelocity();
    }
  }
  
  /** */
  rotateLeft() {
    this.vr = -ROTATE_ANGLE;
  }
  /** */
  rotateRight() {
    this.vr = ROTATE_ANGLE;
  }
  /** */
  zeroRotation() {
    this.vr = 0;
  }

  /** */
  increaseSpeed() {
    this.force = SHIP_FORCE;
    this.showFlame = true;
  }
  /** */
  reverseSpeed() {
    this.force = -SHIP_FORCE/10;
  }
  /** */
  zeroVelocity() {
    this.force = 0;
    this.showFlame = false;
  }

  /** */
  update(elapsed) {
    this.rotation += this.vr * PI_180;

    this.acceleration.x = Math.cos(this.rotation) * this.force;
    this.acceleration.y = Math.sin(this.rotation) * this.force;

    this.velocity.x += this.acceleration.x;
    this.velocity.y += this.acceleration.y;

    this.velocity.x *= this.friction;
    this.velocity.y *= this.friction;

    this.x += this.velocity.x * elapsed;
    this.y += this.velocity.y * elapsed;
  }

  /** */
  build_graphics() {
    cos = Math.cos(drawing_angle * PI_180);
    sin = Math.sin(drawing_angle * PI_180);
    size_2 = this.size/2;
    size_3 = this.size/3;
    size_8 = this.size/8;

    // context.fillStyle = "#e74c3c";
    // context.beginPath();
    // context.moveTo(-size_2, 0)
    // context.lineTo(-size_8,  size_3 * sin)
    // context.lineTo(-size_8, -size_3 * sin)
    // context.closePath();
    // context.fill();
    this.flame_graphic.setAttribute("fill", "#e74c3c");
    this.flame_graphic.setAttribute("d", `M ${-size_2}, 0 
                                          L ${-size_8}, ${ size_3 * sin}
                                          L ${-size_8}, ${-size_3 * sin}
                                          z`);

    // context.fillStyle = "#ecf0f1";
    // context.beginPath();
    // context.moveTo(size_2, 0);
    // context.lineTo(size_2 * cos, size_2 * sin);
    // context.lineTo(-size_8, 0);
    // context.lineTo(size_2 * cos, -size_2 * sin);
    // context.closePath();
    // context.fill();
    this.ship_graphic.setAttribute("fill", "#ecf0f1")
    this.ship_graphic.setAttribute("d", `M ${size_2}, 0
                                         L ${size_2 * cos}, ${size_2 * sin}
                                         L ${-size_8}, 0
                                         L ${size_2 * cos}, ${-size_2 * sin}
                                         z`);
  }
  /** */
  render(context) {
    this.graphics.setAttribute("transform", `translate(${this.x}, ${this.y}) rotate(${rad2Deg(this.rotation)})`);

    this.flame_graphic.style.display = (this.showFlame) ? "block" : "none";
  }
}

function rad2Deg(rad) {
  return rad * 180/Math.PI;
}