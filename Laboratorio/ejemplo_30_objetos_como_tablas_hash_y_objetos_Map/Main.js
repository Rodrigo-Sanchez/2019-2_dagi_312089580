let obj_1 = {
  id: "obj_1_id", 
  value: 4,
};
obj_1.color = "#ff3311";
obj_1.position = {x: 0, y:0};

console.log(obj_1);

console.log("id =",       obj_1.id);
console.log("value =",    obj_1.value);
console.log("color =",    obj_1.color);
console.log("position =", obj_1.position);
console.log("no_esta =",  obj_1.no_esta);

console.log("id =",       obj_1["id"]);
console.log("value =",    obj_1["value"]);
console.log("color =",    obj_1["color"]);
console.log("position =", obj_1["position"]);
console.log("no_esta =",  obj_1["no_esta"]);



let map_1 = new Map();
console.log(map_1);

map_1.set("id", "map_1_id");
map_1.set("value", 189);
map_1.set("color", "azul");
map_1.set("position", {x:10, y:-10});
console.log(map_1);

let map_2 = new Map([
  ["id", "map_2_id"],
  ["value", 972],
  ["color", "green"],
  ["position", {x:2, y:2}]
]);
console.log(map_2);

console.log("id =", map_1.get("id"));
console.log("value =", map_1.get("value"));
console.log("color =", map_1.get("color"));
console.log("position =", map_1.get("position"));
console.log("id =", map_1.get("no_esta"));

map_1.delete("color");
console.log(map_1.has("color"));

map_2.clear();
console.log(map_2);
