window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.lineWidth = 10;
  context.strokeStyle = "purple";
  context.fillStyle = "green";

  context.beginPath();
  context.arc(100, 100, 25, 0, Math.PI);
  context.stroke();

  context.beginPath();
  context.arc(300, 100, 25, 0, Math.PI, true);
  context.stroke();

  context.lineCap = "round";
  context.beginPath();
  context.arc(100, 200, 25, 0, Math.PI);
  context.stroke();
  context.fill();

  context.beginPath();
  context.arc(300, 200, 25, 0, Math.PI);
  context.fill();
  context.stroke();

  context.lineCap = "square";
  context.beginPath();
  context.arc(100, 300, 25, 0, Math.PI);
  context.stroke();
  context.fill();

  context.arc(300, 300, 25, 0, Math.PI);
  context.fill();
  context.stroke();
});