import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.Ball = new Ball(40, 40, 20);

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();

    this.canvas.oncontextmenu = function() { return false; };

    this.canvas.addEventListener("mousedown", (evt) => {
      this.changeBallAcceleration(evt);
    });
    this.canvas.addEventListener("mouseup", () => {
      this.ballAccelerationToZero();
    });
  }

  processInput() {
  }

  update(elapsed) {
    this.Ball.update(elapsed);
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.Ball.render(this.context);
  }

  changeBallAcceleration(evt) {
    this.get_mouse_position(evt);

    let dx = this.mouse_position.x - this.Ball.x;
    let dy = this.mouse_position.y - this.Ball.y;
    let angle = Math.atan2(dy, dx);

    // botón principal
    if (evt.button === 0) {
      this.Ball.setAcceleration(5, angle);
    }
    // botón secundario
    if (evt.button === 2) {
      this.Ball.setAcceleration(-5, angle);
    }
  }

  ballAccelerationToZero() {
    this.Ball.setAcceleration(0, 0);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}