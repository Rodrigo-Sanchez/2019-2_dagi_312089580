export default class Ball {
  constructor(x=0, y=0, radius=1, color="#b71540") {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
    this.angle = 45;
    this.speed = 10;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
  }

  update(elapsed) {
    this.velocity.x = this.velocity.x + this.acceleration.x;
    this.velocity.y = this.velocity.y + this.acceleration.y;

    this.x = this.x + this.velocity.x * elapsed;
    this.y = this.y + this.velocity.y * elapsed;
  }

  render(context) {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI*2);
    context.fillStyle = this.color;    
    context.fill();
  }

  setAngularVelocity(angle) {
    this.velocity.x = this.speed * Math.cos(angle);
    this.velocity.y = this.speed * Math.sin(angle);
  }
  setAcceleration(force, angle) {
    this.acceleration.x = force * Math.cos(angle);
    this.acceleration.y = force * Math.sin(angle);
  }
}

function degreeToRadian(d) {
  return d * Math.PI / 180;
}