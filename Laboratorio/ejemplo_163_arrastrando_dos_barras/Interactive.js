import Bar from "./Bar.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.bar_1 = new Bar(this.canvas.width/2, this.canvas.height/2, 100, 15, "#ecf0f1");
    this.bar_2 = new Bar(this.canvas.width/2, this.canvas.height/2, 100, 15, "#21ecf0");
  }

  processInput() {
  }

  update(elapsed) {
    let dx = this.mouse_position.x - this.bar_1.x;
    let dy = this.mouse_position.y - this.bar_1.y;
    this.bar_1.rotation = Math.atan2(dy, dx);

    let offset_x = this.bar_1.getPin().x - this.bar_1.x;
    let offset_y = this.bar_1.getPin().y - this.bar_1.y;
    this.bar_1.x = this.mouse_position.x - offset_x;
    this.bar_1.y = this.mouse_position.y - offset_y;

    dx = this.bar_1.x - this.bar_2.x;
    dy = this.bar_1.y - this.bar_2.y;
    this.bar_2.rotation = Math.atan2(dy, dx);

    offset_x = this.bar_2.getPin().x - this.bar_2.x;
    offset_y = this.bar_2.getPin().y - this.bar_2.y;
    this.bar_2.x = this.bar_1.x - offset_x;
    this.bar_2.y = this.bar_1.y - offset_y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.bar_1.render(this.context);
    this.bar_2.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
