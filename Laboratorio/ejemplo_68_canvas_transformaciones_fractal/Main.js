window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  function branch(length, angle, scale) {
    context.fillRect(0, 0, 2, length);

    if (length < 8) {
      return;
    }

    context.save();

    context.fillStyle = "red";
    context.translate(0, length);
    context.rotate(-angle);
    branch(length * scale, angle, scale);

    context.fillStyle = "blue";
    context.rotate(2 * angle);
    branch(length * scale, angle, scale);

    context.restore();
  }

  context.translate(canvas.width/2, 0);
  branch(120, 0.5, 0.8);
});
