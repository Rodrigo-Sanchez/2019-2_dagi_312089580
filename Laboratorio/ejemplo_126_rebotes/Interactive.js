import Ball from "./Ball.js";

let left;
let right;
let top;
let bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    left = 0;
    right = this.canvas.width;
    top = 0;
    bottom = this.canvas.height;

    this.ball = new Ball(this.canvas.width/2, this.canvas.height/2, 20);
  }

  processInput() {
  }

  update(elapsed) {
    this.ball.update(elapsed);

    this.checkBoundaries();
  }

  checkBoundaries() {
    if (this.ball.x + this.ball.radius > right) {
      this.ball.velocity.x *= -1;
      this.ball.x = right - this.ball.radius;
    }
    else if (this.ball.x - this.ball.radius < left) {
      this.ball.velocity.x *= -1;
      this.ball.x = left + this.ball.radius;
    }

    if (this.ball.y + this.ball.radius > bottom) {
      this.ball.velocity.y *= -1;
      this.ball.y = bottom - this.ball.radius;
    }
    else if (this.ball.y - this.ball.radius < top) {
      this.ball.velocity.y *= -1;
      this.ball.y = top + this.ball.radius;
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.ball.render(this.context);
  }
}