let str_1 = 'Mambrú se fue a la guerra, qué dolor, qué dolor, qué pena, Mambrú se fue a la guerra, no sé cuándo vendrá.\nDo-re-mi, do-re-fa, no sé cuándo vendrá. oooooooo. 1 2 3 45.';
console.log(str_1);

let regexp_1 = /guerra/;
let regexp_2 = new RegExp("guerra");

/** exec */
console.log("exec: ", regexp_1.exec(str_1));

/** test */
console.log("test: ", regexp_2.test(str_1));


let regexp_3 = /mambrú/;
console.log("---- /mambrú/ ----");
/** match */
console.log("match: ", str_1.match(regexp_3));
/** replace */
console.log("replace: ", str_1.replace(regexp_3, "MAMBRU"));
/** search */
console.log("search: ", str_1.search(regexp_3));


let regexp_4 = /mambrú/i;
console.log("---- /mambrú/i ----");
console.log("match: ", str_1.match(regexp_4));
console.log("replace: ", str_1.replace(regexp_4, "MAMBRU"));
console.log("search: ", str_1.search(regexp_4));


let regexp_5 = /mambrú/ig;
console.log("---- /mambrú/ig ----");
console.log("match: ", str_1.match(regexp_5));
console.log("replace: ", str_1.replace(regexp_5, "MAMBRU"));
console.log("search: ", str_1.search(regexp_5));


let regexp_6 = new RegExp("mambrú", "ig");
console.log('---- new RegExp("mambrú", "ig") ----');
console.log("match: ", str_1.match(regexp_6));
console.log("replace: ", str_1.replace(regexp_6, "MAMBRU"));
console.log("search: ", str_1.search(regexp_6));

let regexp_7 = /(mambrú)/i;
console.log("---- /(mambrú)/i ----");
console.log("match: ", str_1.match(regexp_7));


let regexp_8 = /./ig;
console.log("---- /./ig ----");
console.log("match: ", str_1.match(regexp_8));

let regexp_9 = /\d/ig;
console.log("---- /\d/ig ----");
console.log("match: ", str_1.match(regexp_9));

let regexp_10 = /\D/ig;
console.log("---- /\D/ig ----");
console.log("match: ", str_1.match(regexp_10));

let regexp_11 = /\w/ig; // [A-Za-z0-9_]
console.log("---- /\w/ig ----");
console.log("match: ", str_1.match(regexp_11));

let regexp_12 = /\W/ig;
console.log("---- /\W/ig ----");
console.log("match: ", str_1.match(regexp_12));

let regexp_13 = /\s/ig;
console.log("---- /\s/ig ----");
console.log("match: ", str_1.match(regexp_13));

let regexp_14 = /\S/ig;
console.log("---- /\S/ig ----");
console.log("match: ", str_1.match(regexp_14));


let regexp_15 = /[aeiou]/ig;
console.log("---- /[aeiou]/ig ----");
console.log("match: ", str_1.match(regexp_15));

let regexp_16 = /[^aeiou]/ig;
console.log("---- /[^aeiou]/ig ----");
console.log("match: ", str_1.match(regexp_16));


let regexp_17 = /^Mam/ig;
console.log("---- /^Mam/ig ----");
console.log("match: ", str_1.match(regexp_17));

let regexp_18 = /45.$/ig;
console.log("---- /45\.$/ig ----");
console.log("match: ", str_1.match(regexp_18));


let regexp_19 = /oo*/ig;
console.log("---- /oo*/ig ----");
console.log("match: ", str_1.match(regexp_19));

let regexp_20 = /o+/ig;
console.log("---- /o+/ig ----");
console.log("match: ", str_1.match(regexp_20));

let regexp_21 = /guerr?as/ig;
console.log("---- /guerr?as/ig ----");
console.log("match: ", str_1.match(regexp_21));



let str_2 = "12px solid blue";
console.log("match: ", str_2.match(/(\d+)px/));

let str_3 = "solid 12px red";
console.log("match: ", str_3.match(/(\d+)px/));

let str_4 = "75.5% solid #ffffff";
console.log("match: ", str_4.match(/(\d+)px|([\d\.]+)%/));
console.log("match: ", str_3.match(/(\d+)px|([\d\.]+)%/));


let regexp_22 = /(#\w+)|(rgb\(\d+,\d+,\d+\))|(grey|red|green|blue|white|black)/i;
let str_5 = "75% solid #ffffff";
console.log("match: ", str_5.match(regexp_22));

let str_6 = "solid 75% grey";
console.log("match: ", str_6.match(regexp_22));

let str_7 = "75% rgb(255,0,0) solid";
console.log("match: ", str_7.match(regexp_22));

let str_8 = "75% RGB(0,255,0) solid";
console.log("match: ", str_8.match(regexp_22));


console.log("correo@fciencias.unam.mx".match(/^(\w+[\.-]?\w+)+@(\w+[\.-]?\w+)*\.(\w+)$/))