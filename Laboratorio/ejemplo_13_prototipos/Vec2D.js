function Vec2D(x, y) {
  this.x = x;
  this.y = y;
}

Vec2D.prototype.add = function(v) {
  return new Vec2D(this.x + v.x, this.y + v.y);
}

Vec2D.addStatic = function(u, v) {
  return new Vec2D(u.x + v.x, u.y + v.y);
}