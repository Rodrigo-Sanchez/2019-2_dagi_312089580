import Segment from "./Segment.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment_1 = new Segment(this.canvas.width/3, this.canvas.height/2 +50, 2*this.canvas.width/3, this.canvas.height/2 -50, "#2ecc71");
    this.segment_2 = new Segment(this.canvas.width/2, 200, this.mouse_position.x, this.mouse_position.y, "#3498db");
  }

  processInput() {
  }

  update(elapsed) {
    this.segment_2.x2 = this.mouse_position.x;
    this.segment_2.y2 = this.mouse_position.y;

    this.collisionSegmentSegment(this.segment_1, this.segment_2);
  }

  collisionSegmentSegment(segment_1, segment_2) {
    let side_1 = this.getSide(
      { x: this.segment_1.x1, y: this.segment_1.y1 },
      { x: this.segment_1.x2, y: this.segment_1.y2 },
      { x: this.segment_2.x1, y: this.segment_2.y1 }
    );

    let side_2 = this.getSide(
      { x: this.segment_1.x1, y: this.segment_1.y1 },
      { x: this.segment_1.x2, y: this.segment_1.y2 },
      { x: this.segment_2.x2, y: this.segment_2.y2 }
    );

    let side_3 = this.getSide(
      { x: this.segment_2.x1, y: this.segment_2.y1 },
      { x: this.segment_2.x2, y: this.segment_2.y2 },
      { x: this.segment_1.x1, y: this.segment_1.y1 }
    );

    let side_4 = this.getSide(
      { x: this.segment_2.x1, y: this.segment_2.y1 },
      { x: this.segment_2.x2, y: this.segment_2.y2 },
      { x: this.segment_1.x2, y: this.segment_1.y2 }
    );

    if ( (side_1 !== side_2) && (side_3 !== side_4) ) {
      segment_1.color = segment_2.color = "#2c3e50";
    }
    else {
      segment_1.color = "#2ecc71";
      segment_2.color = "#3498db";
    }      
  }

  getSide(p1, p2, p3) {
    let or = (p2.y - p1.y) * (p3.x - p2.x) - (p2.x - p1.x) * (p3.y - p2.y);

    if (or > 0) {
      return "izquierda";
    }
    else if (or < 0) {
      return "derecha";
    }
    else {
      return "colineal";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.segment_1.render(this.context);
    this.segment_2.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

