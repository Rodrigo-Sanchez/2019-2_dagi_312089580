import Segment from "./Segment.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment_1 = new Segment(this.canvas.width/3, this.canvas.height/2 +50, 2*this.canvas.width/3, this.canvas.height/2 -50, "#2ecc71");
    this.segment_2 = new Segment(this.canvas.width/2, 200, this.mouse_position.x, this.mouse_position.y, "#3498db");

    this.intersection = {x: 0, y: 0, intersects: false};
  }

  processInput() {
  }

  update(elapsed) {
    this.segment_2.x2 = this.mouse_position.x;
    this.segment_2.y2 = this.mouse_position.y;

    this.collisionSegmentSegment(this.segment_1, this.segment_2);
  }

  collisionSegmentSegment(segment_1, segment_2) {
    // pa = p1 + ua * (p2 - p1)
    // pb = p3 + ub * (p4 - p3)
    let x1 = segment_1.x1;
    let y1 = segment_1.y1;
    let x2 = segment_1.x2;
    let y2 = segment_1.y2;

    let x3 = segment_2.x1;
    let y3 = segment_2.y1;
    let x4 = segment_2.x2;
    let y4 = segment_2.y2;

    let ua = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
    let ub = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

    if ( 
      (ua >= 0) && (ua <= 1) &&
      (ub >= 0) && (ub <= 1)
    ) {
      segment_1.color = segment_2.color = "#2c3e50";
      this.intersection.x = x1 + (ua * (x2 - x1));
      this.intersection.y = y1 + (ua * (y2 - y1));
      this.intersection.intersects = true;
    }
    else {
      segment_1.color = "#2ecc71";
      segment_2.color = "#3498db";
      this.intersection.intersects = false;
    }      
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.segment_1.render(this.context);
    this.segment_2.render(this.context);

    if (this.intersection.intersects) {
      this.context.fillStyle = "#f39c12";
      this.context.beginPath();
      this.context.arc(this.intersection.x, this.intersection.y, 5, 0, 2*Math.PI);
      this.context.fill();
    }
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

