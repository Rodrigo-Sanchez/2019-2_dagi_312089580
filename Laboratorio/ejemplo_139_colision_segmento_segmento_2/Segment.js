export default class Segment {
  constructor(x1=0, y1=0, x2=10, y2=10, color="#b71540") {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    
    this.color = color;
  }

  update(elapsed) {
  }

  render(context) {
    context.beginPath();
    context.lineWidth = 4;
    context.strokeStyle = this.color;
    context.moveTo(this.x1, this.y1);
    context.lineTo(this.x2, this.y2);
    context.stroke();
  }
}
