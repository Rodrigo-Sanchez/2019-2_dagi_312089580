window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  function drawBaseLine(x, y, w)  {
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = "gray";
    context.moveTo(x,   y+0.5);
    context.lineTo(x+w, y+0.5);
    context.stroke();
  }

  drawBaseLine(50, 50, 700)
  context.fillStyle = "red";
  context.font = "20px Sans-Serif";
  context.fillText("Un texto relleno", 50, 50);

  drawBaseLine(50, 130, 700)
  context.strokeStyle = "green";
  context.lineWidth = 2;
  context.font = "30px Sans-Serif";
  context.strokeText("Un texto con borde", 50, 130);

  drawBaseLine(50, 210, 700)
  context.strokeStyle = "blue";
  context.fillStyle = "yellow";
  context.lineWidth = 10;
  context.font = "28px Sans-Serif";
  context.strokeText("A veces los textos con bordes tienen picos raros", 50, 210);
  context.fillText("A veces los textos con bordes tienen picos raros", 50, 210);

  drawBaseLine(50, 290, 700)
  context.strokeStyle = "red";
  context.fillStyle = "pink";
  context.lineWidth = 10;
  context.lineJoin = "round";
  context.font = "18px Sans-Serif";
  context.strokeText('Pero se compone con lineJoin = "round" o lineJoin = "bevel"', 50, 290);
  context.fillText('Pero se compone con lineJoin = "round" o lineJoin = "bevel"', 50, 290);

  drawBaseLine(50, 370, 700)
  context.fillStyle = "black";
  context.font = "italic bold 20px Monospace";
  context.fillText("Un texto con italicas y negritas", 50, 370);
});