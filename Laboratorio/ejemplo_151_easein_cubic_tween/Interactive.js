import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.ball = new Ball(this.canvas.width/3, this.canvas.height/2, 20, "rgba(142, 68, 173, 0.75)");

    this.t = 0;
    this.duration = 1;
    
    this.p1 = { x:this.canvas.width/3, y:2*this.canvas.height/3 };
    this.p2 = { x:2*this.canvas.width/3, y:this.canvas.height/3 };
  }

  processInput() {
  }

  update(elapsed) {
    if (this.t < this.duration) {
      this.ball.x = easeInCubic(this.t, this.p1.x, this.p2.x,   this.duration);
      this.ball.y = easeInCubic(this.t, this.p1.y, this.p2.y, this.duration);
    }

    this.t += elapsed;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.context.beginPath();
    this.context.moveTo(this.p1.x, this.p1.y);
    this.context.lineTo(this.p2.x, this.p2.y);
    this.context.stroke();

    this.context.beginPath();
    this.context.fillStyle = "#000000";
    this.context.arc(this.p1.x, this.p1.y, 3, 0, 2*Math.PI);
    this.context.arc(this.p2.x, this.p2.y, 3, 0, 2*Math.PI);
    this.context.fill();

    this.ball.render(this.context);
  }
}

function easeInCubic(time, init, end, duration) {
  return init + (end - init) * Math.pow(time/duration, 3)
}