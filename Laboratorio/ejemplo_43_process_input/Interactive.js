import KEY from "./Key.js";

export default class Interactive {
  constructor(container) {
    this.container = container;

    this.block = document.getElementById("cuadro");

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });

    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  processInput() {
    if (KEY.isPressed(KEY.LEFT)) {
      this.block.textContent = KEY.LEFT;
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.block.textContent = KEY.RIGHT;
    }
    if (KEY.isPressed(KEY.UP)) {
      this.block.textContent = KEY.UP;
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.block.textContent = KEY.DOWN;
    }
  }

  update(elapsed) {
  }

  render() {

  }
}
