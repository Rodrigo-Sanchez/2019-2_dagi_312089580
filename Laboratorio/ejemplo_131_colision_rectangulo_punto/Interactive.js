import Ball from "./Rectangle.js";
import Rectangle from "./Rectangle.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.rectangle = new Rectangle(this.canvas.width/2 -100, this.canvas.height/2-50, 200, 100, "#3498db");
  }

  processInput() {
  }

  update(elapsed) {
    this.collisionRectanglePoint(this.rectangle, this.mouse_position);
  }

  collisionRectanglePoint(rectangle, point) {
    if (
      point.x > rectangle.x  &&  
      point.x < rectangle.x + rectangle.w  &&  
      point.y > rectangle.y  &&  
      point.y < rectangle.y + rectangle.h
    )  {
      this.rectangle.color = "#e67e22";
    }
    else {
      this.rectangle.color = "#3498db";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.rectangle.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
