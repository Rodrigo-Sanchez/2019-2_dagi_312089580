window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  function drawBaseLine(x, y, w)  {
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = "gray";
    context.moveTo(x,   y+0.5);
    context.lineTo(x+w, y+0.5);
    context.stroke();
  }

  context.fillStyle = "black";
  context.font = "20px Sans-Serif";

  drawBaseLine(50, 50, 700)
  context.textBaseline = "top"
  context.fillText('textBaseline = "top"', 50, 50);

  drawBaseLine(50, 130, 700)
  context.textBaseline = "hanging"
  context.fillText('textBaseline = "hanging"', 50, 130);

  drawBaseLine(50, 210, 700)
  context.textBaseline = "middle"
  context.fillText('textBaseline = "middle"', 50, 210);

  drawBaseLine(50, 290, 700)
  context.textBaseline = "alphabetic"
  context.fillText('textBaseline = "alphabetic"', 50, 290);

  drawBaseLine(50, 370, 700)
  context.textBaseline = "ideographic"
  context.fillText('textBaseline = "ideographic"', 50, 370);

  drawBaseLine(50, 450, 700)
  context.textBaseline = "bottom"
  context.fillText('textBaseline = "bottom"', 50, 450);

});