export default class Gato {
  constructor() {
    this.info = document.querySelector("#info");
    this.game_area = document.querySelector("#game_area");
    this.cells = [...this.game_area.querySelectorAll("div")];
    this.retro = document.querySelector("#retro");
    this.retro_txt = document.querySelector("#retro_txt");

    this.current_player = "j1";

    this.game_area.addEventListener("mousedown", (evt) => this.make_move(evt) );

    document.querySelector("#retro_btn").addEventListener("mousedown", () => this.restart() );
  }

  /**
   * Función que realiza un moviento en el tablero
   * @param {Event} evt el evento capturado por el manejador de eventos de mousedonw del área de juego 
   */
  make_move(evt) {
    if (evt.target !== this.game_area) {
      if (!evt.target.hasAttribute("player")) {
        evt.target.setAttribute("player", this.current_player);

        if (this.continue_game()) {
          this.current_player = (this.current_player === "j1") ? "j2" : "j1";
          this.info.textContent = `Es el turno del jugador ${(this.current_player === "j1") ? 1 : 2}`;
        }
      }
    }
  }

  /**
   * Función que reinicia el juego
   */
  restart() {
    this.cells.forEach((cell) => cell.removeAttribute("player"));
    this.retro.style.display = "none";
  }

  /**
   * Función que revisa la configuración del tablero y decide si algún jugador ganó, si hay un empate o si el juego continua
   * @return {Boolean} regresa true si el juego continua y false en otro caso
   */
  continue_game() {
    let winner = this.check_winner();
    if (winner !== 0) {
      this.retro_txt.textContent = `El jugador ${winner} ganó`;
      this.retro.style.display = "block";
      return false;
    }

    if (this.check_tie()) {
      this.retro_txt.textContent = `Ocurrió un empate`;
      this.retro.style.display = "block";
      return false;
    }

    return true;
  }

  /**
   * Función que verifica si algun jugador ganó
   * @return {Number} regresa el indice del jugador que gano o cero si ninguno ha ganado
   */
  check_winner() {
    // 0 | 1 | 2
    // 3 | 4 | 5 
    // 6 | 7 | 8
    return this.check_winner_aux(0, 1, 2) ||
           this.check_winner_aux(3, 4, 5) ||
           this.check_winner_aux(6, 7, 8) ||
           this.check_winner_aux(0, 3, 6) ||
           this.check_winner_aux(1, 4, 7) ||
           this.check_winner_aux(2, 5, 8) ||
           this.check_winner_aux(0, 4, 8) ||
           this.check_winner_aux(2, 4, 6);
  }

  /**
   * Funcion que compara el atributo "player" de tres celdas para saber si son iguales
   * @param {Number} i el indice de la primera celda
   * @param {Number} j el indice de la segunda celda
   * @param {Number} k el indice de la tercera celda
   * @return {Number} regresa el indice del jugador cuyo atributo "player" aparece en las tres celdas o cero en caso de que no
   */
  check_winner_aux(i, j, k) {
    let player_cell_i = this.cells[i].getAttribute("player");
    let player_cell_j = this.cells[j].getAttribute("player");
    let player_cell_k = this.cells[k].getAttribute("player");

    if ( (player_cell_i === player_cell_j) && (player_cell_i === player_cell_k) ) {
      if (player_cell_i === "j1") {
        return 1;
      }
      else if (player_cell_i === "j2") {
        return 2;
      }
    }
    return 0;
  }

  /**
   * Funcion que revisa si cada celda tiene el atributo "player"
   * @return {Boolean} regresa true si el tablero ya esta lleno y false en caso contrario
   */
  check_tie() {
    return this.cells.every((cell) => cell.hasAttribute("player"));
  }
}
