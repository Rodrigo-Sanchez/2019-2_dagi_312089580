const PI_180 = Math.PI/180;
const drawing_angle = 125;
const ROTATE_ANGLE = 3;
const SHIP_FORCE = 20;
let cos;
let sin;
let size_2;
let size_3;
let size_8;

export default class Ship {
  constructor(x=0, y=0, size=1) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.vr = 0;
    this.rotation = 0;
    this.force = 0;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
    this.showFlame = false;
  }

  /** */
  processInput(KEY) {
    if (KEY.isPressed(KEY.LEFT)) {
      this.rotateLeft();
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.rotateRight();
    }
    if (KEY.isPressed(KEY.UP)) {
      this.increaseSpeed();
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.reverseSpeed();
    }

    if (!KEY.isPressed(KEY.LEFT) && !KEY.isPressed(KEY.RIGHT)) {
      this.zeroRotation();
    }
    if ((!KEY.isPressed(KEY.UP)) && !KEY.isPressed(KEY.DOWN)) {
      this.zeroVelocity();
    }
  }
  
  /** */
  rotateLeft() {
    this.vr = -ROTATE_ANGLE;
  }
  /** */
  rotateRight() {
    this.vr = ROTATE_ANGLE;
  }
  /** */
  zeroRotation() {
    this.vr = 0;
  }

  /** */
  increaseSpeed() {
    this.force = SHIP_FORCE;
    this.showFlame = true;
  }
  /** */
  reverseSpeed() {
    this.force = -SHIP_FORCE/10;
  }
  /** */
  zeroVelocity() {
    this.force = 0;
    this.showFlame = false;
  }

  /** */
  update(elapsed) {
    this.rotation += this.vr * PI_180;

    this.acceleration.x = Math.cos(this.rotation) * this.force;
    this.acceleration.y = Math.sin(this.rotation) * this.force;

    this.velocity.x += this.acceleration.x;
    this.velocity.y += this.acceleration.y;

    this.x += this.velocity.x * elapsed;
    this.y += this.velocity.y * elapsed;
  }

   /** */
   render(context) {
    cos = Math.cos(drawing_angle * PI_180);
    sin = Math.sin(drawing_angle * PI_180);
    size_2 = this.size/2;
    size_3 = this.size/3;
    size_8 = this.size/8;

    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.rotation);

    if (this.showFlame) {
      context.fillStyle = "#e74c3c";
      context.beginPath();
      context.moveTo(-size_2, 0)
      context.lineTo(-size_8,  size_3 * sin)
      context.lineTo(-size_8, -size_3 * sin)
      context.closePath();
      context.fill();
    }

    context.fillStyle = "#ecf0f1";
    context.beginPath();
    context.moveTo(size_2, 0);
    context.lineTo(size_2 * cos, size_2 * sin);
    context.lineTo(-size_8, 0);
    context.lineTo(size_2 * cos, -size_2 * sin);
    context.closePath();
    context.fill();

    context.restore();
  }
}
