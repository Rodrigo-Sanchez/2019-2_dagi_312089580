import Ship from "./Ship.js";
import KEY from "./Key.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.Ship = new Ship(this.canvas.width/2, this.canvas.height/2, 40);

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });
    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  processInput() {
    this.Ship.processInput(KEY);
  }

  update(elapsed) {
    this.Ship.update(elapsed);
  }

  render() {
    this.context.fillStyle = "#2c3e50";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.Ship.render(this.context);
  }
}