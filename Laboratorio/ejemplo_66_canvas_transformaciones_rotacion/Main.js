window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "red";
  context.strokeRect(50, 50, 100, 50);

  context.rotate(20*Math.PI/180);
  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "black";
  context.strokeRect(50, 50, 100, 50);
});
