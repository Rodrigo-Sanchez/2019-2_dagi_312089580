import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.circle_1 = new Ball(this.canvas.width/2, this.canvas.height/2, 100, 35000, "#f39c12");
    this.circle_1.velocity = {x: 0, y:0};

    this.circle_2 = new Ball(200, 300, 30, 1, "#27ae60");
    this.circle_2.velocity = {x: 0, y:100};
  }

  processInput() {
  }

  update(elapsed) {
    this.move(this.circle_2, elapsed);
    this.gravitate(this.circle_2, this.circle_1);
  }

  move(particle, elapsed) {
    particle.x += particle.velocity.x * elapsed;
    particle.y += particle.velocity.y * elapsed;
  }

  gravitate(particle_1, particle_2) {
    let dx = particle_2.x - particle_1.x;
    let dy = particle_2.y - particle_1.y;
    let distSQ = dx * dx + dy * dy;
    let force = particle_1.mass * particle_2.mass / distSQ;
    let angle = Math.atan2(dy, dx);
    let ax = force * Math.cos(angle);
    let ay = force * Math.sin(angle);

    particle_1.velocity.x += ax / particle_1.mass;
    particle_1.velocity.y += ay / particle_1.mass;
    particle_2.velocity.x -= ax / particle_2.mass;
    particle_2.velocity.y -= ay / particle_2.mass;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.circle_1.render(this.context);
    this.circle_2.render(this.context);

  }
}
