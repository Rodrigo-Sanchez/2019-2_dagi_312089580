export default class Ball {
  constructor(x=0, y=0, radius=1, mass=1, color="#b71540") {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.mass = mass;
    this.color = color;
  }

  update(elapsed) {
  }

  render(context) {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI*2);
    context.fillStyle = this.color;    
    context.fill();
  }
}
