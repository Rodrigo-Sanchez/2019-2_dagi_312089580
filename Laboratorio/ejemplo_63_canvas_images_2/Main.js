window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  let subimage = {
    x: 149,
    y: 185,
    w: 100,
    h: 100
  }

  let img_1 = document.createElement("img");
  img_1.addEventListener("load", () => {
    console.log(img_1, img_1.width, img_1.height);

    context.drawImage(img_1, 0, 0);
    context.strokeStyle = "black";
    context.strokeRect(0, 0, img_1.width, img_1.height);
    context.strokeStyle = "red";
    context.strokeRect(subimage.x, subimage.y, subimage.w, subimage.h);
  });
  img_1.src = "ciencias.jpg";

  let img_2 = document.createElement("img");
  img_2.addEventListener("load", () => {
    console.log(img_2, img_2.width, img_2.height);

    context.drawImage(
      img_2, 
      subimage.x, subimage.y, // position in image
      subimage.w, subimage.h, // size in image
      500, 10,  // position in canvas
      200, 200  // size in canvas
    );
  });
  img_2.src = "ciencias.jpg";

});