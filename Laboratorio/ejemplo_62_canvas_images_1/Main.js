window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  let img_1 = document.createElement("img");
  img_1.addEventListener("load", () => {
    console.log(img_1, img_1.width, img_1.height);

    context.drawImage(img_1, 10, 10);
  });
  img_1.src = "android.svg";

  let img_2 = document.createElement("img");
  img_2.addEventListener("load", () => {
    console.log(img_2, img_2.width, img_2.height);

    context.drawImage(
      img_2, 
      300, 10, // position
      100, 600 // size
    );
  });
  img_2.src = "ciencias.jpg";

});