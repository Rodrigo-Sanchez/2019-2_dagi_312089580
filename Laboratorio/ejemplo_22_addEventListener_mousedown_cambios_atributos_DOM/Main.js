window.addEventListener("load", function(evt) {
  let container = document.querySelector("#container");

  container.addEventListener("mousedown", function(evt) {
    if (evt.target == container) {
      console.log("clic en el contenedor");

      let children = evt.target.querySelectorAll(".special_div");
      for (var i=0; i<children.length; i++) {
        children[i].removeAttribute("clicked");
        children[i].style.backgroundImage = "";
      }
    }
    else {
      console.log("clic en el hijo");

      if (evt.target.hasAttribute("clicked")) {
        evt.target.style.backgroundImage = "";
        evt.target.removeAttribute("clicked");
      }
      else {
        evt.target.setAttribute("clicked", true);
        evt.target.style.backgroundImage = "url('marca.svg')"; // background-image:url('marca.svg');
      }

      console.log(evt.target.getAttribute("clicked"));
    }
  });
});