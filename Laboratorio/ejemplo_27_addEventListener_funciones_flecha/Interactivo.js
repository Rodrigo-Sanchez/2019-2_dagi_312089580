export default class Interactivo {
  constructor() {
    this.container = document.querySelector("#container");

    this.container.addEventListener("mousedown", (evt) => {
console.log("this =", this);
      if (evt.target == this.container) {
        console.log("clic en el contenedor");
  
        let children = evt.target.querySelectorAll(".special_div");
        for (var i=0; i<children.length; i++) {
          if (children[i].classList.contains("clicked")) {
            children[i].classList.remove("clicked");
          }
        }
      }
      else {
        console.log("clic en el hijo");
        console.log("className =", evt.target.className);
  
        evt.target.classList.toggle("clicked");
      }
    });
  }
}