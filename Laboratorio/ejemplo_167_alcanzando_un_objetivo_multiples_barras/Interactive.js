import Bar from "./Bar.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.bars = [];
    this.num_bars = 5;

    for (let i=0; i<this.num_bars; i++) {
      this.bars.push( new Bar(0, 0, 50, 10, "rgba(142, 68, 173, 0.75)") );
    }

    this.bars[this.num_bars-1].x = this.canvas.width / 2;
    this.bars[this.num_bars-1].y = this.canvas.height / 2;
    this.bars[0].color = "#cc0000";
  }

  processInput() {
  }

  update(elapsed) {
    let target = this.reach(this.bars[0], this.mouse_position.x, this.mouse_position.y);

    this.bars.forEach((bar, index) => {
      if (index != 0) {
        target = this.reach(bar, target.x, target.y);
        this.position(this.bars[index-1], bar);
      }
    });
  }

  reach(bar, xpos, ypos) {
    let dx = xpos - bar.x;
    let dy = ypos - bar.y;

    bar.rotation = Math.atan2(dy, dx);

    let w = bar.getPin().x - bar.x;
    let h = bar.getPin().y - bar.y;

    return {
      x: xpos - w,
      y: ypos - h
    }
  }

  position(bar_1, bar_2) {
    bar_1.x = bar_2.getPin().x;
    bar_1.y = bar_2.getPin().y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.bars.forEach((bar) => {
      bar.render(this.context);
    })
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
