let val;
let A = [9, 8, 7, 6, 5];
console.log(A);

/** some */
val = A.some(function(item, index, array) {
  if (item > 3) {
    return true;
  }
  else {
    return false;
  }
});
console.log("some :", val);

/** every */
val = A.every(function(item, index, array) {
  if (item > 0) {
    return true;
  }
  else {
    return false;
  }
});
console.log("every: ", val);

/** find */
val = A.find(function(item, index, array) {
  if (item >= 3) {
    return true;
  }
  else {
    return false;
  }
});
console.log("find: ", val);

/** findIndex */
val = A.findIndex(function(item, index, array) {
  return (item >= 3);
});
console.log("findIndex: ", val);

/** filter */
val = A.filter(function(item, index, array) {
  return (item >= 3);
});
console.log("filter: ", val);


/** map */
val = A.map(function(item, index, array) {
  return item * 3;
});
console.log("map: ", val, A);


/** reduce */
val = A.reduce(function(acumulator, currentValue, index, array) {
  return acumulator + currentValue;
}, 0);
console.log("reduce: ", val);


/** concat */
val = A.concat([10, 6, 9, 8, 7]);
console.log("concat: ", val, A)


/** fill */
A.fill(-1, 0, 2);
console.log("fill: ", A)

/** reverse */
A.reverse(-1, 0, 2);
console.log("reverse: ", A)


A = [10, 3, -1, 5, 102, -10, 8, 23];
/** sort */
A.sort();
console.log("sort: ", A);

A.sort(function(a, b) {
  if (a < b) 
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
});
console.log("sort: ", A);

A.sort(function(a, b) {
  return a - b;
});
console.log("sort: ", A);


/** join */
console.log("join: ", A.join());

console.log("join: ", A.join(" @ "));

/** Array.from */
A = Array.from("Hola");
console.log("Array.from", A);
