f();
function f() {
  console.log("hola desde la funcion f");
}

// g(); // desde aqui no funciona la funcion no esta definida
var g = function() {
  console.log("hola desde la funcion g");
}
g(); // desde aqui si funciona
