let str_1 = "";
console.log(str_1);

let str_2 = 'El veloz murciélago hindú comía feliz cardillo y kiwi. La cigüeña toca el saxofón detrás del palenque de paja.';
console.log(str_2);

let str_3 = ` La Nueva FoRMa `;
console.log(str_3);

let str_4 = new String(9876543210);
console.log(str_4);


/** charAt */
for (let i=0; i<str_4.length; i++) {
  console.log(`str_4.charAt(${i}) = ${str_4.charAt(i)}, str_4[${i}] = ${str_4[i]}`);
}

/** < y > */
console.log("<: ", str_2 < str_4);
console.log(">: ", str_2 > str_4);


/** concat */
console.log("concat: ", str_3.concat(str_4));
console.log("+: ", str_3 + str_4);


/** includes */
console.log("includes: ", str_2.includes("kiwi"));
console.log("includes: ", str_2.includes("KIWI"));

/** startsWith */
console.log("startsWith: ", str_2.startsWith("El"));
console.log("startsWith: ", str_2.startsWith("el"));

/** endsWith */
console.log("endsWith: ", str_2.endsWith("paja."));
console.log("endsWith: ", str_2.endsWith("pajA."));

/** indexOf */
console.log("indexOf: ", str_2.indexOf("cardillo"));
console.log("indexOf: ", str_2.indexOf("CARDILLO"));

/** lastIndexOf */
console.log("lastIndexOf: ", str_2.lastIndexOf("de"));
console.log("lastIndexOf: ", str_2.lastIndexOf("DE"));

/** repeat */
console.log("repeat: ", str_4.repeat(3));
console.log("repeat: ", "0".repeat(10));

/** slice */
console.log("slice: ", str_2.slice(25));
console.log("slice: ", str_2.slice(10, 16));

/** split */
console.log("split: ", str_3.split(" "));
console.log("split: ", str_2.split("."));

/** substring */
console.log("substring: ", str_2.substring(25));
console.log("substring: ", str_2.substring(10, 16));

/** toLowerCase */
console.log("toLowerCase: ", str_3.toLowerCase());

/** toUpperCase */
console.log("toUpperCase: ", str_3.toUpperCase());

/** trim */
console.log("trim: ", str_3.trim());
