window.addEventListener("load", function(evt) {
  let link = document.getElementById("save");
  let image = document.getElementById("una_imagen");
  
  let canvas = document.createElement("canvas");
  canvas.setAttribute("width", image.width);
  canvas.setAttribute("height", image.height);
  let context = canvas.getContext("2d");
  context.drawImage(image, 0, 0);

  link.addEventListener("click", function(evt) {
    link.href = canvas.toDataURL("image/png");
  })
});