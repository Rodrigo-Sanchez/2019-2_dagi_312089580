let set_1 = new Set();
console.log(set_1);

let set_2 = new Set([1, 2, 9, 8, 1, 7, 2]);
console.log(set_2);

console.log(`set_1.size = ${set_1.size}, set_2.size = ${set_2.size}`);


set_1.add("a");
set_1.add(7);
set_1.add({color:"rojo"});
set_1.add("a");
set_1.add(7);
console.log("add :", set_1);


set_1.delete("a");
console.log("delete :", set_1);


set_1.delete({color:"rojo"});
console.log("delete :", set_1);


let element = {valor: 3};
set_1.add(element);
console.log("add element :", set_1);
set_1.delete(element);
console.log("delete element :", set_1);

console.log("has :", set_1.has(7));

set_1.clear();
console.log("clear :", set_1);


set_2.forEach(function(value) {
  console.log(`value = ${value}`);
});
