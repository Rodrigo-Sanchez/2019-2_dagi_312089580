window.addEventListener("load", function() {
  let container = document.getElementById("container");

  let svg = document.createElement("svg");
  svg.setAttribute("width", 800);
  svg.setAttribute("height", 600);

  let rect = document.createElement("rect");
  rect.setAttribute("x", 10);
  rect.setAttribute("y", 10);
  rect.setAttribute("width", 100);
  rect.setAttribute("height", 100);
  rect.setAttribute("fill", "#aacc33");

  svg.appendChild(rect);

  container.appendChild(svg);
});