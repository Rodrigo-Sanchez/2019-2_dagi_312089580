import {Vec2D, Vec3D as MiNombreParaElVector3D} from "./Vectores.js";

let u = new Vec2D(1, -10);
let v = new Vec2D(5, 2);

let w = new MiNombreParaElVector3D(1, 2, 3);

console.log(u.add(v));

console.log(Vec2D.addStatic(u, v));

console.log(w);