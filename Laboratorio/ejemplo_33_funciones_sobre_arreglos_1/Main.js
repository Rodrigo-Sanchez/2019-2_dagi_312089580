let val;
let A = [9, 8, 7, 6, 5];
console.log(A);

/** Array.isArray */
console.log(Array.isArray(val), Array.isArray(A));

/** forEach */
A.forEach(function(item, index, array) {
  console.log(`item = ${item}, index = ${index}, array = ${array}`);
});


/** push */
A.push(1);
console.log(A);

/** pop */
val = A.pop();
console.log(A, "---->", val);

/** unshift */
A.unshift(10);
console.log(A);

/** shift */
val = A.shift();
console.log(A, "---->", val);

/** indexOf */
val = A.indexOf(7);
console.log(val);

val = A.indexOf(10);
console.log(val);

/** lastIndexOf */
val = A.lastIndexOf(6);
console.log(val);

val = A.lastIndexOf(0);
console.log(val);


/** splice */
let start = 2;
let deleteCount = 0;
A.splice(start, deleteCount, 101, 102);
console.log(A);

A.splice(3, 2);
console.log(A);

A.splice(0, 5, ...[1, 2, 3, 4, 5]);
console.log(A);


