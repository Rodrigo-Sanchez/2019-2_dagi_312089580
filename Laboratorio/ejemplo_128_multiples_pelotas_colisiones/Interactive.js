import Ball from "./Ball.js";

let left;
let right;
let top;
let bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    left = 0;
    right = this.canvas.width;
    top = 0;
    bottom = this.canvas.height;

    let num_balls = 50;
    let x;
    let y;
    let radius;
    let color;
    this.balls = [];

    for (let i=0; i<num_balls; i++) {
      radius = 10 + Math.random() * 25;
      color = `rgba(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}, 1)`;
      x = radius + Math.random()*(right - 2*radius);
      y = radius + Math.random()*(bottom - 2*radius);
      this.balls.push( new Ball(x, y, radius, color) );
    }
  }

  processInput() {
  }

  update(elapsed) {
    this.balls.forEach((ball) => {
      ball.update(elapsed);
      this.checkBoundaries(ball);
    });

    for (let i=0; i<this.balls.length-1; i++) {
      for (let j=i+1; j<this.balls.length; j++) {
        this.checkCollision(this.balls[i], this.balls[j], elapsed);
      }
    }
  }

  checkCollision(ball0, ball1, elapsed) {
    let dx = ball1.x - ball0.x;
    let dy = ball1.y - ball0.y;
    let dist = Math.sqrt(dx * dx + dy * dy);

    if (dist < ball0.radius + ball1.radius) {
      let angle = Math.atan2(dy, dx);
      let sin = Math.sin(angle);
      let cos = Math.cos(angle);

      let x0 = 0;
      let y0 = 0;

      let x1 = dx * cos + dy * sin;
      let y1 = dy * cos - dx * sin;

      let vx0 = ball0.velocity.x * cos + ball0.velocity.y * sin;
      let vy0 = ball0.velocity.y * cos - ball0.velocity.x * sin;

      let vx1 = ball1.velocity.x * cos + ball1.velocity.y * sin;
      let vy1 = ball1.velocity.y * cos - ball1.velocity.x * sin;


      let tmpvx0 = ((ball0.mass - ball1.mass) * vx0 + 2 * ball1.mass * vx1) / (ball0.mass + ball1.mass);
      vx1 = ((ball1.mass - ball0.mass) * vx1 + 2 * ball0.mass * vx0) / (ball0.mass + ball1.mass);
      vx0 = tmpvx0;

      let absV = Math.abs(vx0 * elapsed) + Math.abs(vx1 * elapsed);
      let overlap = (ball0.radius + ball1.radius) - Math.abs(x0 - x1);
      x0 += (vx0 / absV * overlap)* elapsed;
      x1 += (vx1 / absV * overlap)* elapsed;

      let x0Final = x0 * cos - y0 * sin;
      let y0Final = y0 * cos + x0 * sin;
      let x1Final = x1 * cos - y1 * sin;
      let y1Final = y1 * cos + x1 * sin;

      ball1.x = ball0.x + x1Final;
      ball1.y = ball0.y + y1Final;
      ball0.x = ball0.x + x0Final;
      ball0.y = ball0.y + y0Final;

      ball0.velocity.x = vx0 * cos - vy0 * sin;
      ball0.velocity.y = vy0 * cos + vx0 * sin;
      ball1.velocity.x = vx1 * cos - vy1 * sin;
      ball1.velocity.y = vy1 * cos + vx1 * sin;
    }
  }

  checkBoundaries(ball) {
    if (ball.x + ball.radius > right) {
      ball.velocity.x *= -1;
      ball.x = right - ball.radius;
    }
    else if (ball.x - ball.radius < left) {
      ball.velocity.x *= -1;
      ball.x = left + ball.radius;
    }

    if (ball.y + ball.radius > bottom) {
      ball.velocity.y *= -1;
      ball.y = bottom - ball.radius;
    }
    else if (ball.y - ball.radius < top) {
      ball.velocity.y *= -1;
      ball.y = top + ball.radius;
    }
  }

  render() {
    this.context.fillStyle = "#ecf0f1";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.balls.forEach((ball) => {
      ball.render(this.context);
    });

  }
}