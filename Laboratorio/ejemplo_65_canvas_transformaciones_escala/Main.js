window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "red";
  context.arc(50, 50, 40, 0, 7);
  context.stroke();

  context.scale(3, 0.5);
  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "black";
  context.arc(50, 50, 40, 0, 7);
  context.stroke();
});
