export default class Model {
  constructor() { }

  init(controller) {
    this.canvas = document.getElementById("drawing");
    this.context = this.canvas.getContext("2d");

    this.aux_canvas = document.createElement("canvas");
    this.aux_context = this.aux_canvas.getContext("2d");

    this.controller = controller;
  }

  setImage(image) {
    this.canvas.setAttribute("width", image.width);
    this.canvas.setAttribute("height", image.height);
    this.aux_canvas.setAttribute("width", image.width);
    this.aux_canvas.setAttribute("height", image.height);

    this.context.drawImage(image, 0, 0);

    this.undo_step = 0;
    this.undo_list = [image];
  }

  getImage() {
    return this.canvas.toDataURL("image/png");
  }

  putImage(image) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.drawImage(image, 0, 0);

    let img = document.createElement("img");
    img.src = this.canvas.toDataURL("image/png");

    this.undo_step++;
    this.undo_list[this.undo_step] = img;
  }

  undo() {
    if (this.undo_step > 0) {
      this.undo_step--;

      this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.context.drawImage(this.undo_list[this.undo_step], 0, 0);
    }
  }

  getColorIndex(x, y, width) {
    let color_pos = x*4 + y*(width * 4);
    return { R: color_pos, G: color_pos+1, B: color_pos+2, A: color_pos+3 };
  }

  apply_function_per_pixel(fun) {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let color_index;
    let new_color;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        new_color = fun( imageData.data[color_index.R], imageData.data[color_index.G], imageData.data[color_index.B] );
        
        imageData.data[color_index.R] = new_color.R;
        imageData.data[color_index.G] = new_color.G;
        imageData.data[color_index.B] = new_color.B;
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  grayscale_action() {
    let gray;
    this.apply_function_per_pixel(function(R, G, B) {
      gray = (R + G + B)/3;
      return { R: gray, G: gray, B: gray };
    });
  }

  invert_action() {
    this.apply_function_per_pixel(function(R, G, B) {
      return { R: 255 - R, G: 255 - G, B: 255 - B };
    });
  }

  red_channel_action() {
    this.apply_function_per_pixel(function(R, G, B) {
      return { R: R, G: 0, B: 0 };
    });
  }

  green_channel_action() {
    this.apply_function_per_pixel(function(R, G, B) {
      return { R: 0, G: G, B: 0 };
    });
  }

  blue_channel_action() {
    this.apply_function_per_pixel(function(R, G, B) {
      return { R: 0, G: 0, B: B };
    });
  }

  gamma_correction_action() {
    let gama_value = 0.7;
    this.apply_function_per_pixel(function(R, G, B) {
      return { 
        R: 255 * Math.pow((R/256), gama_value), 
        G: 255 * Math.pow((G/256), gama_value), 
        B: 255 * Math.pow((B/256), gama_value)
      };
    });
  }


  apply_convolution(matrix) {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let new_imageData = this.context.createImageData(this.canvas.width, this.canvas.height);

    let margin_x = Math.floor(matrix.size_n/2);
    let margin_y = Math.floor(matrix.size_m/2);
    let entry_x, entry_y;
    let color_index;
    let new_color;

    for (let i=margin_x; i<this.canvas.width-margin_x; i++) {
      for (let j=margin_y; j<this.canvas.height-margin_y; j++) {
        new_color = { R:0, G:0, B:0 };

        for (let entry=0; entry<matrix.values.length; entry++) {
          entry_x = (entry%matrix.size_n) -margin_x;
          entry_y = Math.floor(entry/matrix.size_n) -margin_y;

          color_index = this.getColorIndex(i + entry_x, j + entry_y, this.canvas.width);
          
          new_color.R += imageData.data[color_index.R] * matrix.values[entry];
          new_color.G += imageData.data[color_index.G] * matrix.values[entry];
          new_color.B += imageData.data[color_index.B] * matrix.values[entry];
        }

        color_index = this.getColorIndex(i, j, this.canvas.width);
        new_imageData.data[color_index.R] = new_color.R;
        new_imageData.data[color_index.G] = new_color.G;
        new_imageData.data[color_index.B] = new_color.B;
        new_imageData.data[color_index.A] = imageData.data[color_index.A];
      }
    }

    this.aux_context.putImageData(new_imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  border_detection_action() {
    let matrix = {
      values : 
      [
        0, -1,  0,
       -1,  4, -1,
        0, -1,  0
     ],
      size_n : 3, size_m : 3
    }

    this.apply_convolution(matrix);
  }

  soft_action() {
    let matrix = {
      values : 
      [
       1/9, 1/9, 1/9,
       1/9, 1/9, 1/9,
       1/9, 1/9, 1/9
     ],
      size_n : 3, size_m : 3
    }

    this.apply_convolution(matrix);
  }

}