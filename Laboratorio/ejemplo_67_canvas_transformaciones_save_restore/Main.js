window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.strokeStyle = "red";

  context.save();
  context.beginPath();
  context.lineWidth = 3;
  context.strokeRect(50, 50, 100, 50);

  context.rotate(20*Math.PI/180);
  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "black";
  context.strokeRect(50, 50, 100, 50);
  context.restore();

  context.beginPath();
  context.lineWidth = 3;
  context.strokeStyle = "blue";
  context.arc(150, 150, 40, 0, 7);
  context.stroke();
});
