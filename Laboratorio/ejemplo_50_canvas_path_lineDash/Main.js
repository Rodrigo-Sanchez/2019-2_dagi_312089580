window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.lineWidth = 10;

  context.setLineDash([1, 1]);
  context.beginPath();
  context.moveTo(50, 50);
  context.lineTo(750, 50);
  context.stroke();

  context.setLineDash([10, 15]);
  context.beginPath();
  context.moveTo(50, 150);
  context.lineTo(750, 150);
  context.stroke();

  context.setLineDash([10, 15]);
  context.lineCap = "round";
  context.beginPath();
  context.moveTo(50, 250);
  context.lineTo(750, 250);
  context.stroke();

  context.setLineDash([10, 15]);
  context.lineDashOffset = 7;
  context.lineCap = "round";
  context.beginPath();
  context.moveTo(50, 350);
  context.lineTo(750, 350);
  context.stroke();
});