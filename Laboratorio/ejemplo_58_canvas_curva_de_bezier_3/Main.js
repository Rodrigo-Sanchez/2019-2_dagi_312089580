window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;
  let max_elapsed_wait = 30/1000;
  let counter_time = 0;
  let time_step = 0.05;

  let init_point = { x: 200, y: 100 };
  let control_point_1 = { x: 150, y: 300 };
  let control_point_2 = { x: 350, y: 410 };
  let end_point = { x: 500, y: 175 };

  let parts = 100;
  let counter = 0;

  function draw() {
    // track time
    current = Date.now();
    elapsed = (current - lastTime) / 1000;
    if (elapsed > max_elapsed_wait) {
      elapsed = max_elapsed_wait;
    }

    // clear the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    // draw the support lines
    context.lineWidth = 1;
    context.setLineDash([8, 5]);
    context.strokeStyle = "gray";
    context.beginPath();
    context.moveTo(init_point.x, init_point.y);
    context.lineTo(control_point_1.x, control_point_1.y);
    context.lineTo(control_point_2.x, control_point_2.y);
    context.lineTo(end_point.x, end_point.y);
    context.stroke();

    context.setLineDash([]);


    // draw the bezier curve
    context.lineWidth = 10;
    context.strokeStyle = "rgba(100,0,0,0.5)";
    context.beginPath();
    context.moveTo(init_point.x, init_point.y);
    context.bezierCurveTo(
      control_point_1.x, control_point_1.y,
      control_point_2.x, control_point_2.y, 
      end_point.x, end_point.y
    );
    context.lineTo(end_point.x, end_point.y);
    context.stroke();

    // auxiliary points
    let aux_point_1 = {
      x: init_point.x + counter * (control_point_1.x - init_point.x)/parts,
      y: init_point.y + counter * (control_point_1.y - init_point.y)/parts
    }
    let aux_point_2 = {
      x: control_point_1.x + counter * (control_point_2.x - control_point_1.x)/parts,
      y: control_point_1.y + counter * (control_point_2.y - control_point_1.y)/parts
    }
    let aux_point_3 = {
      x: control_point_2.x + counter * (end_point.x - control_point_2.x)/parts, 
      y: control_point_2.y + counter * (end_point.y - control_point_2.y)/parts
    }

    // draw the construction
    context.lineWidth = 2;
    context.strokeStyle = "blue";
    context.beginPath();
    context.moveTo( aux_point_1.x, aux_point_1.y );
    context.lineTo( aux_point_2.x, aux_point_2.y );
    context.stroke();
  
    context.strokeStyle = "red";
    context.beginPath();
    context.moveTo( aux_point_2.x, aux_point_2.y );
    context.lineTo( aux_point_3.x, aux_point_3.y );
    context.stroke();



    let aux_point_4 = {
      x: aux_point_1.x + counter * (aux_point_2.x - aux_point_1.x)/parts,
      y: aux_point_1.y + counter * (aux_point_2.y - aux_point_1.y)/parts
    }
    let aux_point_5 = {
      x: aux_point_2.x + counter * (aux_point_3.x - aux_point_2.x)/parts,
      y: aux_point_2.y + counter * (aux_point_3.y - aux_point_2.y)/parts
    }

    context.strokeStyle = "purple";
    context.beginPath();
    context.moveTo( aux_point_4.x, aux_point_4.y );
    context.lineTo( aux_point_5.x, aux_point_5.y );
    context.stroke();



    let aux_point_final = {
      x: aux_point_4.x + counter * (aux_point_5.x - aux_point_4.x)/parts,
      y: aux_point_4.y + counter * (aux_point_5.y - aux_point_4.y)/parts
    }

    context.fillStyle = "black";
    context.beginPath();
    context.arc( aux_point_final.x, aux_point_final.y, 5, 0, 2*Math.PI);
    context.fill();

    
    if (counter_time > time_step) {
      counter = (counter+1)%parts;
      counter_time = 0;
    }

    counter_time += elapsed;


    lastTime = current;
    window.requestAnimationFrame(draw);
  }

  window.requestAnimationFrame(draw);

});