const PI_180 = Math.PI/180;
const ROTATE_ANGLE = 3;
const SHIP_FORCE = 20;

export default class Ship {
  constructor(container, x=0, y=0, size=1) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.vr = 0;
    this.rotation = 0;
    this.force = 0;
    this.friction = 0.95;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
    this.showFlame = false;

    this.graphics = container.appendChild( document.createElement("div") );
    this.graphics.setAttribute("style", `position:relative; left:-${size/2}px; top:-${size/2}px; transform-origin:center; width:${size}px; height:${size}px;`);
    this.flame_graphic = this.graphics.appendChild( document.createElement("img") );
    this.flame_graphic.src = "img/flame.png";
    this.flame_graphic.setAttribute("style", `position:absolute; width:100%; height:100%;`);
    this.ship_graphic = this.graphics.appendChild( document.createElement("img") );
    this.ship_graphic.src = "img/ship.png";
    this.ship_graphic.setAttribute("style", `position:absolute; width:100%; height:100%;`);
  }

  /** */
  processInput(KEY) {
    if (KEY.isPressed(KEY.LEFT)) {
      this.rotateLeft();
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.rotateRight();
    }
    if (KEY.isPressed(KEY.UP)) {
      this.increaseSpeed();
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.reverseSpeed();
    }

    if (!KEY.isPressed(KEY.LEFT) && !KEY.isPressed(KEY.RIGHT)) {
      this.zeroRotation();
    }
    if ((!KEY.isPressed(KEY.UP)) && !KEY.isPressed(KEY.DOWN)) {
      this.zeroVelocity();
    }
  }
  
  /** */
  rotateLeft() {
    this.vr = -ROTATE_ANGLE;
  }
  /** */
  rotateRight() {
    this.vr = ROTATE_ANGLE;
  }
  /** */
  zeroRotation() {
    this.vr = 0;
  }

  /** */
  increaseSpeed() {
    this.force = SHIP_FORCE;
    this.showFlame = true;
  }
  /** */
  reverseSpeed() {
    this.force = -SHIP_FORCE/10;
  }
  /** */
  zeroVelocity() {
    this.force = 0;
    this.showFlame = false;
  }

  /** */
  update(elapsed) {
    this.rotation += this.vr * PI_180;

    this.acceleration.x = Math.cos(this.rotation) * this.force;
    this.acceleration.y = Math.sin(this.rotation) * this.force;

    this.velocity.x += this.acceleration.x;
    this.velocity.y += this.acceleration.y;

    this.velocity.x *= this.friction;
    this.velocity.y *= this.friction;

    this.x += this.velocity.x * elapsed;
    this.y += this.velocity.y * elapsed;
  }

  /** */
  render(context) {
    this.graphics.style.transform = `translate(${this.x}px, ${this.y}px) rotate(${this.rotation}rad)`;
    this.flame_graphic.style.display = (this.showFlame) ? "block" : "none";
  }
}
