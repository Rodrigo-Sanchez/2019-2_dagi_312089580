import Ship from "./Ship.js";
import KEY from "./Key.js";

let left;
let right;
let top;
let bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.drawing = document.getElementById("drawing");
    let w = this.drawing.offsetWidth;
    let h = this.drawing.offsetHeight;

    this.Ship = new Ship(this.drawing, w/2, h/2, 40);

    left = 0;
    right = w;
    top = 0;
    bottom = h;

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });
    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  processInput() {
    this.Ship.processInput(KEY);
  }

  update(elapsed) {
    this.Ship.update(elapsed);

    this.checkBoundaries();
  }

  checkBoundaries() {
    if (this.Ship.x > right) {
      this.Ship.x = left;
    }
    else if (this.Ship.x < left) {
      this.Ship.x = right;
    }

    if (this.Ship.y > bottom) {
      this.Ship.y = top;
    }
    else if (this.Ship.y < top) {
      this.Ship.y = bottom;
    }
  }

  render() {
    this.Ship.render();
  }
}