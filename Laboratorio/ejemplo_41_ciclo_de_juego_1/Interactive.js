export default class Interactive {
  constructor(container) {
    this.container = container;

    this.block = document.getElementById("cuadro");
    this.block.x = 0;
    this.block.y = 0;
    this.block.speed = 100;
  }

  processInput() {
    
  }

  update(elapsed) {
    this.block.x += this.block.speed * elapsed;
    this.block.y += this.block.speed * elapsed;

    this.block.style.left = this.block.x + "px";
    this.block.style.top = this.block.x + "px";
  }

  render() {

  }
}
