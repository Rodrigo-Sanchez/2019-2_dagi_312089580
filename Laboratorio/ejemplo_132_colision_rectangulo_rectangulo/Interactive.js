import Ball from "./Rectangle.js";
import Rectangle from "./Rectangle.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.rectangle_1 = new Rectangle(this.canvas.width/2 -100, this.canvas.height/2-50, 200, 100, "#3498db");
    this.rectangle_2 = new Rectangle(this.mouse_position.x, this.mouse_position.y, 50, 50, "#27ae60");
  }

  processInput() {
  }

  update(elapsed) {
    this.rectangle_2.x = this.mouse_position.x - this.rectangle_2.w/2;
    this.rectangle_2.y = this.mouse_position.y - this.rectangle_2.h/2;

    this.collisionRectangleRectangle(this.rectangle_1, this.rectangle_2);
  }

  collisionRectangleRectangle(rectangle_1, rectangle_2) {
    let r1_left   = rectangle_1.x;
    let r1_right  = rectangle_1.x + rectangle_1.w;
    let r1_top    = rectangle_1.y;
    let r1_bottom = rectangle_1.y + rectangle_1.h;

    let r2_left   = rectangle_2.x;
    let r2_right  = rectangle_2.x + rectangle_2.w;
    let r2_top    = rectangle_2.y;
    let r2_bottom = rectangle_2.y + rectangle_2.h;

    if (
      r1_right  > r2_left &&
      r1_left   < r2_right &&
      r1_bottom > r2_top &&
      r1_top    < r2_bottom
    ) {
      this.rectangle_1.color = "#e67e22";
    }
    else {
      this.rectangle_1.color = "#3498db";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.rectangle_1.render(this.context);
    this.rectangle_2.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
