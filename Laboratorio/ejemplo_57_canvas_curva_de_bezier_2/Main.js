window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  let init_point = { x: 200, y: 100 };
  let control_point_1 = { x: 150, y: 300 };
  let control_point_2 = { x: 350, y: 410 };
  let end_point = { x: 500, y: 175 };

  let parts = 5;

  context.strokeStyle = "blue";
  context.beginPath();
  for (let i=1; i<parts; i++) {
    context.moveTo( init_point.x + i * (control_point_1.x - init_point.x)/parts, init_point.y + i * (control_point_1.y - init_point.y)/parts );
    context.lineTo( control_point_1.x + i * (control_point_2.x - control_point_1.x)/parts, control_point_1.y + i * (control_point_2.y - control_point_1.y)/parts );
  }
  context.stroke();

  context.strokeStyle = "red";
  context.beginPath();
  for (let i=1; i<parts; i++) {
    context.moveTo( control_point_1.x + i * (control_point_2.x - control_point_1.x)/parts, control_point_1.y + i * (control_point_2.y - control_point_1.y)/parts );
    context.lineTo( control_point_2.x + i * (end_point.x - control_point_2.x)/parts, control_point_2.y + i * (end_point.y - control_point_2.y)/parts );
  }
  context.stroke();



  context.lineWidth = 1;
  context.setLineDash([8, 5]);
  context.strokeStyle = "gray";
  context.beginPath();
  context.moveTo(init_point.x, init_point.y);
  context.lineTo(control_point_1.x, control_point_1.y);
  context.lineTo(control_point_2.x, control_point_2.y);
  context.lineTo(end_point.x, end_point.y);
  context.stroke();

  context.lineWidth = 10;
  context.setLineDash([]);
  context.strokeStyle = "rgba(100,0,0,0.5)";
  context.beginPath();
  context.moveTo(init_point.x, init_point.y);
  context.bezierCurveTo(
    control_point_1.x, control_point_1.y,
    control_point_2.x, control_point_2.y, 
    end_point.x, end_point.y
  );
  context.lineTo(end_point.x, end_point.y);
  context.stroke();

});