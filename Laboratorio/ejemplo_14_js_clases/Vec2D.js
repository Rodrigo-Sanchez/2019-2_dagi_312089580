export default class Vec2D {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  add(v) {
    return new Vec2D(this.x + v.x, this.y + v.y);
  }

  static addStatic(u, v) {
    return new Vec2D(u.x + v.x, u.y + v.y);
  }
}
