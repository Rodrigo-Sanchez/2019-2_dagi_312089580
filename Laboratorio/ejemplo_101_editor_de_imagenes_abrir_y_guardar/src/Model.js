export default class Model {
  constructor() { }

  init(controller) {
    this.canvas = document.getElementById("drawing");
    this.context = this.canvas.getContext("2d");

    this.controller = controller;
  }

  setImage(image) {
    this.context.drawImage(image, 0, 0);
  }

  getImage() {
    return this.canvas.toDataURL("image/png");
  }
}