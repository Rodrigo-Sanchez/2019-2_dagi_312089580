export default class View {
  constructor() { }

  init(controller) {
    this.controller = controller;

    let svg_document = document.getElementById("toolbar").getSVGDocument()
    let toolbar = svg_document.querySelector("svg");

    toolbar.getElementById("btn_open").addEventListener("click", (evt) => {
      this.controller.open();
    });

    toolbar.getElementById("btn_save").addEventListener("click", (evt) => {
      this.controller.save();
    });

    this.work_area = document.getElementById("work_area");
    this.canvas = document.getElementById("drawing");
  }

  setTitle(tit) {
    let title = document.querySelector("title");
    if (!title) {
      title = document.createElement("title");
      document.head.appendChild(title);
    }

    title.textContent = tit;
  }

  changeSize(w, h) {
    this.canvas.setAttribute("width", w);
    this.canvas.setAttribute("height", h);

    this.canvas.style.display = "block";
    this.canvas.style.left = (this.work_area.offsetWidth  - w)/2 + "px";
    this.canvas.style.top  = (this.work_area.offsetHeight - h)/2 + "px";
  }
}