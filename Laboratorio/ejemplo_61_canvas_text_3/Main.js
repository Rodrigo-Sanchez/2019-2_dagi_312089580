window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  let x = canvas.width/2+0.5;
  context.beginPath();
  context.lineWidth = 1;
  context.strokeStyle = "gray";
  context.moveTo(x, 0);
  context.lineTo(canvas.width/2+0.5, canvas.height);
  context.stroke();

  context.fillStyle = "black";
  context.font = "20px Sans-Serif";

  context.textAlign = "left"
  context.fillText('textAlign = "left"', x, 50);

  context.textAlign = "right"
  context.fillText('textAlign = "right"', x, 130);

  context.textAlign = "center"
  context.fillText('textAlign = "center"', x, 210);

  context.textAlign = "start"
  context.fillText('textAlign = "start"', x, 290);

  context.textAlign = "end"
  context.fillText('textAlign = "end"', x, 370);

});