import Ball from "./Ball.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.Ball = new Ball(this.canvas.width/2, 40, 20);

    this.canvas.addEventListener("mousedown", () => {
      this.goUp();
    });
    this.canvas.addEventListener("mouseup", () => {
      this.releaseGoUp();
    });
  }

  processInput() {
  }

  update(elapsed) {
    this.Ball.update(elapsed);
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.Ball.render(this.context);
  }

  goUp() {
    this.Ball.goUp();
  }

  releaseGoUp() {
    this.Ball.releaseGoUp();
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}