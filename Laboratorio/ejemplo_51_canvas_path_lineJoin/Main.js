window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.lineWidth = 30;

  context.beginPath();
  context.moveTo(50,  75);
  context.lineTo(100, 25);
  context.lineTo(150, 75);
  context.lineTo(200, 25);
  context.lineTo(250, 75);
  context.stroke();

  context.beginPath();
  context.lineJoin = "miter";
  context.moveTo(50,  175);
  context.lineTo(100, 125);
  context.lineTo(150, 175);
  context.lineTo(200, 125);
  context.lineTo(250, 175);
  context.stroke();

  context.beginPath();
  context.lineJoin = "round";
  context.moveTo(50,  275);
  context.lineTo(100, 225);
  context.lineTo(150, 275);
  context.lineTo(200, 225);
  context.lineTo(250, 275);
  context.stroke();

  context.beginPath();
  context.lineJoin = "bevel";
  context.moveTo(50,  375);
  context.lineTo(100, 325);
  context.lineTo(150, 375);
  context.lineTo(200, 325);
  context.lineTo(250, 375);
  context.stroke();
});