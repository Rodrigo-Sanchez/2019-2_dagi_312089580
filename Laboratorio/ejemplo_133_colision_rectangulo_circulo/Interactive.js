import Ball from "./Ball.js";
import Rectangle from "./Rectangle.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.rectangle = new Rectangle(this.canvas.width/2 -100, this.canvas.height/2-50, 200, 100, "#3498db");
    this.ball = new Ball(this.mouse_position.x, this.mouse_position.y, 30, "#27ae60");

    this.nearest_x = this.nearest_y = 0;
  }

  processInput() {
  }

  update(elapsed) {
    this.ball.x = this.mouse_position.x;
    this.ball.y = this.mouse_position.y;

    this.collisionRectangleCircle(this.rectangle, this.ball);
  }

  collisionRectangleCircle(rectangle, ball) {
    this.nearest_x = Math.max( rectangle.x, Math.min(ball.x, rectangle.x + rectangle.w) );
    this.nearest_y = Math.max( rectangle.y, Math.min(ball.y, rectangle.y + rectangle.h) );

    if (distance(ball.x, ball.y, this.nearest_x, this.nearest_y) < ball.radius ) {
      this.rectangle.color = "#e67e22";
    }
    else {
      this.rectangle.color = "#3498db";
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.rectangle.render(this.context);
    this.ball.render(this.context);

    this.context.beginPath();
    this.context.fillStyle = "black";
    this.context.arc(this.nearest_x, this.nearest_y, 2, 0, 2*Math.PI);
    this.context.fill();
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}
