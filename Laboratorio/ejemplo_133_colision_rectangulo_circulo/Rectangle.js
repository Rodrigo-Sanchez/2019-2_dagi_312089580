export default class Rectangle {
  constructor(x=0, y=0, w=10, h=10, color="#b71540") {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.color = color;
  }

  update(elapsed) {
  }

  render(context) {
    context.beginPath();
    context.rect(this.x, this.y, this.w, this.h);
    context.fillStyle = this.color;    
    context.fill();
  }
}