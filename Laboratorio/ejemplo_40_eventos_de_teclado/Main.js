window.addEventListener("load", function(evt) {
  let container = document.querySelector("#container");

  window.addEventListener("keydown", (evt) => {
    console.log(`${evt.type} event : evt.key = ${evt.key}`);
  });

  window.addEventListener("keypress", (evt) => {
    console.log(`${evt.type} event : evt.key = ${evt.key}`);
  });

  window.addEventListener("keyup", (evt) => {
    console.log(`${evt.type} event : evt.key = ${evt.key}`);
  });

  container.addEventListener("keydown", (evt) => {
    console.log(`EVENTO DESDE EL CONTENEDOR ${evt.type} event : evt.key = ${evt.key}`);
    evt.preventDefault();
    evt.stopPropagation();
  });
});