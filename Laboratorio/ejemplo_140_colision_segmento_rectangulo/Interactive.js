import Segment from "./Segment.js";
import Rectangle from "./Rectangle.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.rectangle = new Rectangle(this.canvas.width/3, 200, this.canvas.width/3, 100, "#2ecc71");
    this.segment = new Segment(this.canvas.width/4, this.canvas.height/2 +50, this.mouse_position.x, this.mouse_position.y, "#3498db");

    this.intersection_1 = {x: 0, y: 0, intersects: false};
    this.intersection_2 = {x: 0, y: 0, intersects: false};
  }

  processInput() {
  }

  update(elapsed) {
    this.segment.x2 = this.mouse_position.x;
    this.segment.y2 = this.mouse_position.y;

    this.collisionSegmentRectangle(this.segment, this.rectangle);
  }

  collisionSegmentRectangle(segment, rectangle) {
    this.intersection_1.intersects = false;
    this.intersection_2.intersects = false;

    // left
    let temp_intersection = this.collisionSegmentSegment(segment, {
      x1: rectangle.x,
      y1: rectangle.y,
      x2: rectangle.x,
      y2: rectangle.y + rectangle.h
    });

    if (temp_intersection != null) {
      this.intersection_1.x = temp_intersection.x;
      this.intersection_1.y = temp_intersection.y;
      this.intersection_1.intersects = true;
    }

    // right
    temp_intersection = this.collisionSegmentSegment(segment, {
      x1: rectangle.x + rectangle.w,
      y1: rectangle.y,
      x2: rectangle.x + rectangle.w,
      y2: rectangle.y + rectangle.h
    });
    if (temp_intersection != null) {
      if (this.intersection_1.intersects === false) {
        this.intersection_1.x = temp_intersection.x;
        this.intersection_1.y = temp_intersection.y;
        this.intersection_1.intersects = true;
      }
      else {
        this.intersection_2.x = temp_intersection.x;
        this.intersection_2.y = temp_intersection.y;
        this.intersection_2.intersects = true;
      }
    }
    
    // top
    temp_intersection = this.collisionSegmentSegment(segment, {
      x1: rectangle.x,
      y1: rectangle.y,
      x2: rectangle.x + rectangle.w,
      y2: rectangle.y
    });
    if (temp_intersection != null) {
      if (this.intersection_1.intersects === false) {
        this.intersection_1.x = temp_intersection.x;
        this.intersection_1.y = temp_intersection.y;
        this.intersection_1.intersects = true;
      }
      else {
        this.intersection_2.x = temp_intersection.x;
        this.intersection_2.y = temp_intersection.y;
        this.intersection_2.intersects = true;
      }
    }

    // bottom
    temp_intersection = this.collisionSegmentSegment(segment, {
      x1: rectangle.x,
      y1: rectangle.y + rectangle.h,
      x2: rectangle.x + rectangle.w,
      y2: rectangle.y + rectangle.h
    });
    if (temp_intersection != null) {
      if (this.intersection_1.intersects === false) {
        this.intersection_1.x = temp_intersection.x;
        this.intersection_1.y = temp_intersection.y;
        this.intersection_1.intersects = true;
      }
      else {
        this.intersection_2.x = temp_intersection.x;
        this.intersection_2.y = temp_intersection.y;
        this.intersection_2.intersects = true;
      }
    }

    if (this.intersection_1.intersects || this.intersection_2.intersects) {
      rectangle.color = "#2c3e50";
    }
    else {
      rectangle.color = "#2ecc71";
    }   
  }

  collisionSegmentSegment(segment_1, segment_2) {
    // pa = p1 + ua * (p2 - p1)
    // pb = p3 + ub * (p4 - p3)
    let x1 = segment_1.x1;
    let y1 = segment_1.y1;
    let x2 = segment_1.x2;
    let y2 = segment_1.y2;

    let x3 = segment_2.x1;
    let y3 = segment_2.y1;
    let x4 = segment_2.x2;
    let y4 = segment_2.y2;

    let ua = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
    let ub = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

    if ( (ua >= 0) && (ua <= 1) && (ub >= 0) && (ub <= 1) ) {
      return {
        x: x1 + (ua * (x2 - x1)),
        y: y1 + (ua * (y2 - y1))
      }
    }
    return null;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.rectangle.render(this.context);
    this.segment.render(this.context);

    if (this.intersection_1.intersects) {
      this.context.fillStyle = "#f39c12";
      this.context.beginPath();
      this.context.arc(this.intersection_1.x, this.intersection_1.y, 5, 0, 2*Math.PI);
      this.context.fill();
    }
    if (this.intersection_2.intersects) {
      this.context.fillStyle = "#f39c12";
      this.context.beginPath();
      this.context.arc(this.intersection_2.x, this.intersection_2.y, 5, 0, 2*Math.PI);
      this.context.fill();
    }
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

