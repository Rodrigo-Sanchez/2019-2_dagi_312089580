import Bar from "./Bar.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.bar = new Bar(this.canvas.width/2, this.canvas.height/2, 150, 15, "#ecf0f1");
  }

  processInput() {
  }

  update(elapsed) {
    this.bar.rotation = Math.atan2(this.mouse_position.y - this.bar.y, this.mouse_position.x - this.bar.x);
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.bar.render(this.context);
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
