import Bar from "./Bar.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.bars = [];
    let num_bars = 20;
    let color;

    for (let i=0; i<num_bars; i++) {
      color = `rgba(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}, 1)`;

      this.bars[i] = new Bar(0, 0, 50, 20, color);
    }
  }

  processInput() {
  }

  update(elapsed) {
    this.bars.forEach((bar, index) => {
      if (index === 0) {
        this.drag(bar, this.mouse_position.x, this.mouse_position.y);
      }
      else {
        this.drag(bar, this.bars[index-1].x, this.bars[index-1].y);
      }
    });
  }

  drag(bar, pos_x, pos_y) {
    let dx = pos_x - bar.x;
    let dy = pos_y - bar.y;
    bar.rotation = Math.atan2(dy, dx);

    let offset_x = bar.getPin().x - bar.x;
    let offset_y = bar.getPin().y - bar.y;
    bar.x = pos_x - offset_x;
    bar.y = pos_y - offset_y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.bars.forEach((bar) => {
      bar.render(this.context);
    });
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}
