window.addEventListener("load", function(evt) {
  let container_1 = document.getElementById("container");
  console.log(container_1);

  let container_2 = document.querySelector("#container");
  console.log(container_2);

  let divs_1 = document.getElementsByClassName("special_div");
  console.log(divs_1);

  let divs_2 = document.getElementsByTagName("div");
  console.log(divs_2);

  let divs_3 = document.querySelectorAll(".special_div");
  console.log(divs_3);

  let divs_4 = document.querySelectorAll("div");
  console.log(divs_4);

  console.log(this);
});