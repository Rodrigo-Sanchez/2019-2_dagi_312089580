window.addEventListener("load", function() {
  let viewer = document.getElementById("viewer");
  let back = document.getElementById("back");
  let next = document.getElementById("next");
  let current_section = 0;
  let num_sections = 4;
  let w = 450;

  back.addEventListener("click", function(evt) {
    if (current_section > 0) {
      current_section--;
      viewer.style.left = -(current_section * w) + "px";
    }
  });
  next.addEventListener("click", function(evt) {
    if (current_section < num_sections-1) {
      current_section++;
      viewer.style.left = -(current_section * w) + "px";
    }
  });
});