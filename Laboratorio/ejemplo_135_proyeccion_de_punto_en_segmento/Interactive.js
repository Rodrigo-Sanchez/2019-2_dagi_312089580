import Segment from "./Segment.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousemove", (evt) => {
      this.get_mouse_position(evt);
    });

    this.segment = new Segment(this.canvas.width/3, this.canvas.height/2 -50, 2*this.canvas.width/3, this.canvas.height/2 +50, "#3498db");
    this.projection = {x:0, y:0, in_segment:false};
  }

  processInput() {
  }

  update(elapsed) {
    this.get_projection(this.mouse_position.x, this.mouse_position.y);
  }

  get_projection(x, y) {
    let segment_length = distance(this.segment.x1, this.segment.y1, this.segment.x2, this.segment.y2);

    let segment_unit_vector_x = (this.segment.x2 - this.segment.x1)/segment_length;
    let segment_unit_vector_y = (this.segment.y2 - this.segment.y1)/segment_length;

    let dot = dotProduct(segment_unit_vector_x, segment_unit_vector_y, x - this.segment.x1, y - this.segment.y1);

    this.projection.x = this.segment.x1 + dot * segment_unit_vector_x;
    this.projection.y = this.segment.y1 + dot * segment_unit_vector_y;

    if ((dot >= 0) && (dot <= segment_length)) {
      this.projection.in_segment = true;
    }
    else {
      this.projection.in_segment = false;
    }
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.segment.render(this.context);

    this.context.beginPath();
    this.context.fillStyle = "#000000";
    this.context.arc(this.mouse_position.x, this.mouse_position.y, 5, 0, Math.PI*2);
    this.context.fill();

    if (this.projection.in_segment) {
      this.context.beginPath();
      this.context.fillStyle = "orange";
      this.context.arc(this.projection.x, this.projection.y, 5, 0, Math.PI*2);
      this.context.fill();
    }
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}

function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}

function dotProduct(x1, y1, x2, y2) {
  return x1*x2 + y1*y2;
}