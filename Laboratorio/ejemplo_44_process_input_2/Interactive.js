import KEY from "./Key.js";

export default class Interactive {
  constructor(container) {
    this.container = container;

    this.block = document.getElementById("cuadro");

    this.speed = 50;
    this.direction = KEY.RIGHT;
    this.block_pos = {x: 0, y: 0};

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });

    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  processInput() {
    if (KEY.isPressed(KEY.LEFT)) {
      this.direction = KEY.LEFT;
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.direction = KEY.RIGHT;
    }
    if (KEY.isPressed(KEY.UP)) {
      this.direction = KEY.UP;
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.direction = KEY.DOWN;
    }
  }

  update(elapsed) {
    switch (this.direction) {
      case KEY.LEFT:
        this.block_pos.x -= this.speed * elapsed;
        break;
      case KEY.RIGHT:
        this.block_pos.x += this.speed * elapsed;
        break;
      case KEY.UP:
        this.block_pos.y -= this.speed * elapsed;
        break;
      case KEY.DOWN:
        this.block_pos.y += this.speed * elapsed;
        break;
    }

    this.block.style.left = this.block_pos.x +"px";
    this.block.style.top = this.block_pos.y +"px";
  }

  render() {

  }
}
