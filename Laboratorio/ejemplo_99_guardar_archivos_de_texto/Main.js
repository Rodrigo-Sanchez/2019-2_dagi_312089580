window.addEventListener("load", function(evt) {
  let link = document.getElementById("save");
  let blob = new Blob(["Ejemplo de contenido", " (que podría estar partido en varias partes)"], { type: "text/plain;charset=utf-8;"});
  
  link.addEventListener("click", function(evt) {
    link.href = URL.createObjectURL(blob);
  })
});