window.addEventListener("load", function(evt) {
  let canvas = document.getElementById("the_canvas");
  let context = canvas.getContext("2d");

  context.lineWidth = 50;

  context.beginPath();
  context.moveTo(60, 50);
  context.lineTo(500, 50);
  context.stroke();

  context.beginPath();
  context.lineCap = "butt";
  context.moveTo(60, 150);
  context.lineTo(500, 150);
  context.stroke();

  context.beginPath();
  context.lineCap = "round";
  context.moveTo(60, 250);
  context.lineTo(500, 250);
  context.stroke();

  context.beginPath();
  context.lineCap = "square";
  context.moveTo(60, 350);
  context.lineTo(500, 350);
  context.stroke();
});