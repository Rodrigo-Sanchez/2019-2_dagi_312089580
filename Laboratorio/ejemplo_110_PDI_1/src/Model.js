export default class Model {
  constructor() { }

  init(controller) {
    this.canvas = document.getElementById("drawing");
    this.context = this.canvas.getContext("2d");

    this.aux_canvas = document.createElement("canvas");
    this.aux_context = this.aux_canvas.getContext("2d");

    this.controller = controller;
  }

  setImage(image) {
    this.canvas.setAttribute("width", image.width);
    this.canvas.setAttribute("height", image.height);
    this.aux_canvas.setAttribute("width", image.width);
    this.aux_canvas.setAttribute("height", image.height);

    this.context.drawImage(image, 0, 0);

    this.undo_step = 0;
    this.undo_list = [image];
  }

  getImage() {
    return this.canvas.toDataURL("image/png");
  }

  putImage(image) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.drawImage(image, 0, 0);

    let img = document.createElement("img");
    img.src = this.canvas.toDataURL("image/png");

    this.undo_step++;
    this.undo_list[this.undo_step] = img;
  }

  undo() {
    if (this.undo_step > 0) {
      this.undo_step--;

      this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.context.drawImage(this.undo_list[this.undo_step], 0, 0);
    }
  }

  getColorIndex(x, y, width) {
    let color_pos = x*4 + y*(width * 4);
    return { R: color_pos, G: color_pos+1, B: color_pos+2, A: color_pos+3 };
  }

  grayscale_action() {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let R, G, B, gray;
    let color_index;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        
        R = imageData.data[color_index.R];
        G = imageData.data[color_index.G];
        B = imageData.data[color_index.B];

        gray = (R + G + B)/3;

        imageData.data[color_index.R] = gray;
        imageData.data[color_index.G] = gray;
        imageData.data[color_index.B] = gray;
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  invert_action() {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let color_index;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        imageData.data[color_index.R] = 255 - imageData.data[color_index.R];
        imageData.data[color_index.G] = 255 - imageData.data[color_index.G];
        imageData.data[color_index.B] = 255 - imageData.data[color_index.B];
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  red_channel_action() {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let color_index;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        imageData.data[color_index.G] = 0;
        imageData.data[color_index.B] = 0;
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  green_channel_action() {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let color_index;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        imageData.data[color_index.R] = 0;
        imageData.data[color_index.B] = 0;
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

  blue_channel_action() {
    let imageData = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    let color_index;

    for (let i=0; i<this.canvas.width; i++) {
      for (let j=0; j<this.canvas.height; j++) {
        color_index = this.getColorIndex(i, j, this.canvas.width);
        imageData.data[color_index.R] = 0;
        imageData.data[color_index.G] = 0;
      }
    }

    this.aux_context.putImageData(imageData, 0, 0);
    this.putImage(this.aux_canvas);
  }

}