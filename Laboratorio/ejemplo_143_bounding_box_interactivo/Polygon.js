export default class Polygon {
  constructor(points=[{x:0, y:0}, {x:30, y:30}, {x:30, y:-30}], color="#b71540") {
    this.points = points;
    this.color = color;
  }

  update(elapsed) {
  }

  render(context) {
    context.beginPath();
   
    this.points.forEach((point, index) => {
      if (index === 0) {
        context.moveTo(point.x, point.y);
      }
      else {
        context.lineTo(point.x, point.y);
      }
    });

    context.closePath();
    context.fillStyle = this.color;    
    context.fill();
  }
}