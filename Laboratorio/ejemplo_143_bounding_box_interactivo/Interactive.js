import Polygon from "./Polygon.js";
import Rectangle from "./Rectangle.js";
import Point from "./Point.js";

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.mouse_position = {x: 0, y:0};
    this.rect = this.canvas.getBoundingClientRect();
    window.addEventListener("mousedown", (evt) => {
      this.select_point(evt);
    });
    window.addEventListener("mousemove", (evt) => {
      this.move_point(evt);
    });
    window.addEventListener("mouseup", (evt) => {
      this.release_point(evt);
    });

    let point_size = 5;
    this.points = [
      new Point(100, 100, point_size),
      new Point(350, 80,  point_size),
      new Point(700, 140, point_size),
      new Point(500, 500, point_size),
      new Point(350, 200, point_size),
      new Point(130, 300, point_size)
    ];

    this.polygon = new Polygon(this.points, "#2ecc71");

    this.bbox = new Rectangle(0, 0, 0, 0, "#3498db");

    this.point_selected = undefined;
  }

  processInput() {
  }

  update(elapsed) {
    this.getBoundingBox();
  }

  getBoundingBox() {
    let min_x = Infinity;
    let max_x = -Infinity;
    let min_y = Infinity;
    let max_y = -Infinity;

    this.points.forEach((point) => {
      min_x = Math.min(point.x, min_x);
      max_x = Math.max(point.x, max_x);
      min_y = Math.min(point.y, min_y);
      max_y = Math.max(point.y, max_y);
    });

    this.bbox.x = min_x;
    this.bbox.y = min_y;
    this.bbox.w = max_x - min_x;
    this.bbox.h = max_y - min_y;
  }

  render() {
    this.context.fillStyle = "#ffffff";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

    this.polygon.render(this.context);
    this.bbox.render(this.context);

    this.points.forEach((point) => {
      point.render(this.context);
    });
  }

  collisionCirclePoint(circle, point) {
    return (distance(circle.x, circle.y, point.x, point.y) < circle.radius);
  }

  select_point(evt) {
    this.get_mouse_position(evt);

    this.point_selected = this.points.find( (point) => {
      return this.collisionCirclePoint(point, this.mouse_position);
    });
  }

  move_point(evt) {
    if (this.point_selected) {
      this.get_mouse_position(evt);
      this.point_selected.x = this.mouse_position.x;
      this.point_selected.y = this.mouse_position.y;
    }
  }

  release_point(evt) {
    this.point_selected = undefined;
  }

  get_mouse_position(evt) {
    this.mouse_position.x = evt.clientX - this.rect.left;
    this.mouse_position.y = evt.clientY - this.rect.top;
  }
}


function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx*dx + dy*dy);
}