// Código JS para la práctica 1.

// Variable para controlar si el juego está activo.
game = true;

function change() {
    // Obtenemos el id del div que estamos cliqueando.
    id = change.caller.arguments[0].target.id;
    // Obtenemos el letrero del turno correspondiente.
    sign = document.getElementById("turn");
    // Obtenemos el elemento de la casilla 
    box = document.getElementById(id);
    // Obtenemos el elemento con el id.
    logo = document.getElementById("logo");
    // La ruta completa de la imagen.
    image = logo.src;
    // Recortamos la sección de la ruta que nos interesa.
    imagePath = image.substring(image.length - 22, image.length);
    // Creamos un nuevo nodo de una imagen.
    newImage = document.createElement("img");
    // Obtenemos el jugador actual.
    player = box.getAttribute("value");

    // Revisamos que no hayamos tirado antes en esa casilla.
    if (!player) {
        if (imagePath == "webroot/img/circle.svg") {
            // Agregamos el texto del turno del jugador respectivo.
            sign.textContent = "Es turno del jugador 2 ";
            // Establecemos la imagen del turno del jugador correspondiente.
            newImage.setAttribute("src", "webroot/img/cross.svg");
            // Agregamos información 
            box.setAttribute("value", "O");
            box.style.backgroundImage = "url('webroot/img/circle.svg')";
        } else {
            // Agregamos el texto del turno del jugador respectivo.
            sign.textContent = "Es turno del jugador 1 ";
            // Establecemos la imagen del turno del jugador correspondiente.
            newImage.setAttribute("src", "webroot/img/circle.svg");
            // Agregamos información 
            box.setAttribute("value", "X");
            box.style.backgroundImage = "url('webroot/img/cross.svg')";
        }
    } else {
        // Mandamos un mensaje al usuario.
        alert("Ya has tirado en esta casilla. 🙄");
    }

    // Le volvemos a asignar el id.
    newImage.setAttribute("id", "logo");
    // Le volvemos a as.
    newImage.setAttribute("height", "50vw");
    // Agregamos el nodo newImage al texto del turno del jugador respectivo.
    sign.appendChild(newImage);
    // Hacemos que la imagen de la tirada correspondiente cubra el div.
    box.style.backgroundSize = "cover";

    // Lista para guardar los valores de las tiradas.
    listMoves = [];

    // Obtenemos los divs asociados al contenedor del gato.
    container = document.getElementById("container").children;
    for (let box of container) {
        listMoves.push(box.getAttribute("value"));
    }

    // Obtenemos el jugador actual.
    actualPlayer = box.getAttribute("value");

    win(listMoves, actualPlayer);
}

function reset() {
    window.location.reload();
}

function win(moves, gamer) {
    retro = document.getElementById("retro");
    winner = document.getElementById("winner");
    player = (gamer == "O") ? 1 : 2;

    if (moves[0] === gamer && moves[1] === gamer && moves[2] === gamer && 
        moves[0] !== null && moves[1] !== null && moves[2] !== null) {
        // retro.style.display = "";
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[0] === gamer && moves[3] === gamer && moves[6] === gamer && 
                moves[0] !== null && moves[3] !== null && moves[6] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[0] === gamer && moves[4] === gamer && moves[8] === gamer && 
                moves[0] !== null && moves[4] !== null && moves[8] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[1] === gamer && moves[4] === gamer && moves[7] === gamer && 
                moves[1] !== null && moves[4] !== null && moves[7] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[2] === gamer && moves[4] === gamer && moves[6] === gamer && 
                moves[2] !== null && moves[4] !== null && moves[6] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[2] === gamer && moves[5] === gamer && moves[8] === gamer && 
                moves[2] !== null && moves[5] !== null && moves[8] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[3] === gamer && moves[4] === gamer && moves[5] === gamer && 
                moves[3] !== null && moves[4] !== null && moves[5] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[6] === gamer && moves[7] === gamer && moves[8] === gamer && 
                moves[6] !== null && moves[7] !== null && moves[8] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "El jugador " + player + " ganó.";
    } else if (moves[1] !== null && moves[2] !== null && moves[3] !== null && 
                moves[4] !== null && moves[5] !== null && moves[6] !== null && 
                moves[7] !== null && moves[8] !== null && moves[0] !== null) {
        retro.removeAttribute("style");
        winner.textContent = "¡Empate!";
    }
}