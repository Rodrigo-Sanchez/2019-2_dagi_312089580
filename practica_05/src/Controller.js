export default class Controller {
  constructor() {}

  init(model, view) {
    this.model = model;
    this.view = view;
  }

  open() {
    let file_input = document.createElement("input");
    file_input.setAttribute("type", "file");
    file_input.setAttribute("accept", "image/jpeg,image/png,image/svg");

    file_input.addEventListener("change", (evt) => {
      let file = file_input.files[0];

      const reader = new FileReader();
      reader.addEventListener("load", (elem) => {
        let image = document.createElement("img");

        image.addEventListener("load", () => {
          this.view.setTitle(file.name);
          this.view.changeSize(image.width, image.height);
          this.model.setImage(image);
        });
        image.src = reader.result;
      });
      if (file) {
        reader.readAsDataURL(file);
      }
    });

    file_input.click();
  }

  save() {
    let link = document.createElement("a");
    link.setAttribute("download", "image.png");
    document.body.appendChild(link);

    link.addEventListener("click", (evt) => {
      link.href = this.model.getImage();
      document.body.removeChild(link);
    });

    link.click();
  }

  undo() {
    this.model.undo();
  }

  redo() {
    this.model.redo();
  }

  setStrokeColor(color) {
    this.strokeColor = color;
  }

  /**
   * Función que asigna el color de relleno.
   * @param {DOMString} color 
   */
  setFillColor(color) {
    this.fillColor = color;
  }

  line_mode() {
    this.mode = "line";
  }

  free_mode() {
    this.mode = "free";
  }

  /**
   * Función que asigna el modo rect.
   */
  rect_mode() {
    this.mode = "rect";
  }

  /**
   * Función que asigna el modo rect_fill.
   */
  rect_fill_mode() {
    this.mode = "rect_fill";
  }

  /**
   * Función que asigna el modo circ.
   */
  circ_mode() {
    this.mode = "circ";
  }

  /**
   * Función que asigna el modo circ_fill.
   */
  circ_fill_mode() {
    this.mode = "circ_fill";
  }

  /**
   * Función que se manda a llamar cuando el mouse da click.
   * @param {object} evt 
   * @param {object} canvas 
   */
  mouse_down(evt, canvas) {
    if ((this.mode === "line") || (this.mode === "free") || (this.mode === "rect") ||
      (this.mode === "rect_fill") || (this.mode === "circ") || (this.mode === "circ_fill")) {
      this.rect = canvas.getBoundingClientRect();
      this.init_x = evt.clientX - this.rect.left;
      this.init_y = evt.clientY - this.rect.top;

      this.view.setStrokeColor(this.strokeColor);
      this.view.setFillColor(this.fillColor);
    }

    if (this.mode === "free") {
      this.view.initFree(this.init_x, this.init_y);
    }
  }

  /**
   * Función que manda a llamar la acción de la vista correspondiente de 
   * acuerdo a this.mode cuando el mouse se mueve.
   * @param {object} evt 
   */
  mouse_move(evt) {
    if (this.mode === "line") {
      this.view.drawLine(
        this.init_x, this.init_y,
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    } else if (this.mode === "free") {
      this.view.continueFree(
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    } else if (this.mode === "rect") {
      this.view.drawRect(
        this.init_x, this.init_y,
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    } else if (this.mode === "rect_fill") {
      this.view.drawRectFill(
        this.init_x, this.init_y,
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    } else if (this.mode === "circ") {
      this.view.drawCirc(
        this.init_x, this.init_y,
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    } else if (this.mode === "circ_fill") {
      this.view.drawCircFill(
        this.init_x, this.init_y,
        evt.clientX - this.rect.left,
        evt.clientY - this.rect.top
      );
    }
  }

  mouse_up(aux_canvas) {
    this.model.putImage(aux_canvas);

    this.view.clear();
  }
}