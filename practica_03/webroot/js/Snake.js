import KEY from "./Key.js";

const CELL = '1';
const SNAKE_HEAD = '2';
const SNAKE_BODY = '3';
const FOOD = '4';

const HEIGHT = 48;
const WIDTH = 64;
const SQUARE = 12;

export default class Interactive {
    constructor() {
        this.canvas = document.querySelector("#the_canvas");
        this.context = this.canvas.getContext("2d");
        this.counter = document.querySelector("#counter");
        this.retro = document.querySelector("#retro");
        this.retro_txt = document.querySelector("#retro_txt");
        this.retro_btn = document.querySelector("#retro_btn");

        this.game = true;

        this.score = 0;
        this.direction = KEY.UP;

        this.snakeHead = WIDTH * (HEIGHT / 2) + WIDTH / 2;
        this.snake = [this.snakeHead];
        this.snake.x = WIDTH/2;
        this.snake.y = HEIGHT/2;

        this.food = [this.generateFood(true), this.generateFood(false)];

        this.retro_btn.addEventListener("mousedown", () => this.restart());

        this.background_audio = new Audio("webroot/audio/snake_runner.mp3");
        this.background_audio.loop = true;
        this.background_audio.volume = 0.5;
        this.background_audio.addEventListener("canplay", function() {
            this.play();
        });

        this.eat_audio = new Audio("webroot/audio/level_up.mp3");
        this.eat_audio.volume = 0.5;

        this.lose_audio = new Audio("webroot/audio/game_over.mp3");
        this.lose_audio.volume = 1;

        window.addEventListener("keydown", (evt) => {
            KEY.onKeyDown(evt.key);
        });

        window.addEventListener("keyup", (evt) => {
            KEY.onKeyUp(evt.key);
        });
    }

    processInput() {
        if (this.game) {
            this.snakeCrashing();
            this.snakeEats();
        }
    }

    update(elapsed) {
        if (this.game) {
            this.updateSnakeDirection();
            this.moveSnake();
        }
    }

    render() {
        if (this.game) {
            this.updateBoard();
        }
    }

    /**
     * Función que actualiza la dirección en la cual se mueve la serpiente.
     */
    updateSnakeDirection() {
        if (KEY.isPressed(KEY.LEFT) && this.direction != KEY.RIGHT) {
            this.direction = KEY.LEFT;
        }
        if (KEY.isPressed(KEY.RIGHT) && this.direction != KEY.LEFT) {
            this.direction = KEY.RIGHT;
        }
        if (KEY.isPressed(KEY.UP) && this.direction != KEY.DOWN) {
            this.direction = KEY.UP;
        }
        if (KEY.isPressed(KEY.DOWN) && this.direction != KEY.UP) {
            this.direction = KEY.DOWN;
        }
    }

    /**
     * Función que mueve la cabeza de la serpiente en el tablero.
     */
    moveSnake() {
        switch (this.direction) {
            case KEY.LEFT:
                this.snake.x--;
                break;
            case KEY.RIGHT:
                this.snake.x++;
                break;
            case KEY.UP:
                this.snake.y--;
                break;
            case KEY.DOWN:
                this.snake.y++;
                break;
        }
        this.snake.unshift([this.snake.x, this.snake.y]);
        this.snake.pop();
    }

    /**
     * Función que actualiza el pintado del tablero.
     */
    updateBoard() {
        this.gridPaint();
        this.boardPaint(FOOD, this.context, this.food[0] * SQUARE, this.food[1] * SQUARE);
        this.boardPaint(SNAKE_HEAD, this.context, this.snake.x * SQUARE, this.snake.y * SQUARE);
        this.snakeBodyPaint();
    }

    /**
     * Fucnión que pinta el tablero vacío,
     */
    gridPaint() {
        for (var i = 0; i < WIDTH; i++) {
            for (var j = 0; j < HEIGHT; j++) {
                this.boardPaint(CELL, this.context, i * SQUARE, j * SQUARE);
            }
        }
    }

    /**
     * Función que pinta el cuerpo de la serpiente-
     */
    snakeBodyPaint() {
        let snakeBody = JSON.parse(JSON.stringify(this.snake));
        snakeBody.shift();
        snakeBody.forEach((cell, i) => {
            this.boardPaint(SNAKE_BODY, this.context, snakeBody[i][0] * SQUARE, snakeBody[i][1] * SQUARE);
        });
    }


    /**
     * Funcion que pinta el tablero.
     * @param {String} cell la cadena correspondiente a la i-ésima entrada del arreglo.
     */
    boardPaint(option, context, x, y) {
        switch(option) {
            case CELL:
                context.fillStyle = '#0f380f';
                context.fillRect(x, y, SQUARE, SQUARE);
                context.strokeStyle = '#306230';
                context.strokeRect(x, y, SQUARE, SQUARE); 
                break;
            case SNAKE_HEAD:
                context.fillStyle = '#9bbc0f';
                context.fillRect(x, y, SQUARE, SQUARE);
                context.strokeStyle = 'black';
                context.strokeRect(x, y, SQUARE, SQUARE); 
                break;
            case SNAKE_BODY:
                context.fillStyle = '#9bbc0f';
                context.fillRect(x, y, SQUARE, SQUARE);
                context.strokeStyle = '#8bac0f';
                context.strokeRect(x, y, SQUARE, SQUARE); 
                break;
            case FOOD:
                context.fillStyle = '#e74c3c';
                context.fillRect(x, y, SQUARE, SQUARE);
                context.strokeStyle = 'black';
                context.strokeRect(x, y, SQUARE, SQUARE); 
                break;
        }
    }

    /**
     * Función que genera una posición segura para asignar la comida.
     */
    generateFood(axis) {
        let position = axis ? Math.floor(Math.random() * WIDTH) : Math.floor(Math.random() * HEIGHT);
        while (this.snake.includes(position)) {
            position = axis ? Math.floor(Math.random() * WIDTH) : Math.floor(Math.random() * HEIGHT);
        }
        return position;
    }

    /**
     * Función que aumenta el contador de puntos cuando la serpiente come.
     */
    snakeEats() {
        if (this.snake.x === this.food[0] && this.snake.y === this.food[1]) {
            this.counter.innerHTML = ++this.score;
            this.snake.unshift([this.snake.x, this.snake.y]);
            this.eat_audio.pause();
            this.eat_audio.currentTime = 0;
            this.eat_audio.play();
            this.food = [this.generateFood(true), this.generateFood(false)];
        }
    }

    /**
     * Función que detecta cuando la serpiente choca.
     */
    snakeCrashing() {
        if(this.direction === KEY.LEFT && this.snake.x === 0 ||
            this.direction === KEY.RIGHT && this.snake.x === WIDTH - 1 ||
            this.direction === KEY.UP && this.snake.y === 0 ||
            this.direction === KEY.DOWN && this.snake.y === HEIGHT - 1) {
            this.retro.style.display = 'block';
            this.lose_audio.play();
            this.background_audio.pause();
            this.game = false;
        }

        let snakeBody = JSON.parse(JSON.stringify(this.snake));
        snakeBody.shift();
        snakeBody.forEach((cell, i) => {
            if(snakeBody[i][0] === this.snake.x && snakeBody[i][1] === this.snake.y) {
                this.retro.style.display = 'block';
                this.lose_audio.play();
                this.background_audio.pause();
                this.game = false;
            }
        });
    }

    /**
     * Función que reinicia el juego.
     */
    restart() {
        window.location.reload();
    }
}