export default class Ball {
  constructor(x = 0, y = 0, radius = 1, color = "#black") {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
  }

  /**
   * 
   * @param {*} elapsed 
   * @param {*} x 
   * @param {*} y 
   */
  update(elapsed, x, y) {
    this.x = x;
    this.y = y;
  }

  /**
   * 
   * @param {*} ctx 
   */
  render(ctx) {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    ctx.fillStyle = this.color;
    ctx.fill();
  }
}

/**
 * 
 * @param {*} d 
 */
function degreeToRadian(d) {
  return d * Math.PI / 180;
}