export default class Segment {
  constructor(x1 = 0, y1 = 0, x2 = 10, y2 = 10, color = "white") {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    this.color = color;
  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {

  }

  /**
   * 
   * @param {*} ctx 
   */
  render(ctx) {
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.strokeStyle = this.color;
    ctx.moveTo(this.x1, this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.stroke();
  }
}
