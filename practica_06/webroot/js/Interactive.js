import Ball from "./Ball.js";
import KEY from "./Key.js";
import Particle from "./Particle.js";
import Segment from "./Segment.js";
import Ship from "./Ship.js";

let left, right, top, bottom;

export default class Interactive {
  constructor(container) {
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.ctx = this.canvas.getContext("2d");
    this.retro = document.querySelector("#retro");
    this.retro_txt = document.querySelector("#retro_txt");
    this.retro_btn = document.querySelector("#retro_btn");
    this.retro_btn.addEventListener("mousedown", () => this.restart());

    this.ship = new Ship(this.canvas.width / 2, 0, 40);
    this.circle = new Ball(this.ship.x, this.ship.y, this.ship.size / 2);
    this.projection = { x: 0, y: 0, in_segment: false };
    this.points = [];
    this.distanceX = 55;
    this.pointX = -10;
    this.pointY;
    for (let i = 0; i < 8; i++) {
      this.pointY = Math.floor(Math.random() * 200) + 400;
      this.points[i] = { x: this.pointX, y: this.pointY };
      this.pointX += this.distanceX;
    }
    this.pointY = Math.floor(Math.random() * 200) + 400;
    this.safe = Math.floor(Math.random() * 6);
    this.points[this.safe].y = this.pointY;
    this.points[this.safe + 1].y = this.pointY;
    this.points[this.safe + 2].y = this.pointY;
    this.segments = [];
    for (let i = 0; i < 7; i++) {
      this.segments[i] = new Segment(this.points[i].x, this.points[i].y, this.points[i + 1].x, this.points[i + 1].y);
    }
    this.particles = [];
    this.particle_count = 100;
    for (var i = 0; i < this.particle_count; i++) {
      this.particles.push(new Particle());
    }

    this.game = true;
    this.winner = true;
  
    left = 0;
    right = this.canvas.width;
    top = 0;
    bottom = this.canvas.height;

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });
    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });
  }

  /**
   * 
   */
  processInput() {
    this.ship.processInput(KEY);
  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {
    if(this.game) {
      this.circle.update(elapsed, this.ship.x, this.ship.y);
      this.ship.update(elapsed);
      this.checkBoundaries();
      if(this.checkLanding()) {
        this.game = false;
        this.winner = true;
        this.retro_txt.innerHTML = "¡Ganaste!";
        this.retro.style.display = 'block';
      } else {
        this.segments.forEach((item) => {
          this.collisionSegmentCircle(item, this.circle);
        });    
      }
      this.particles.forEach((item) => {
        item.update(elapsed, this.ship.x, this.ship.y);
      });
    }
  }

  /**
   * 
   */
  render() {
    this.ctx.globalCompositeOperation = "source-over";
    this.ctx.fillStyle = "black";
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.globalCompositeOperation = "lighter";
    this.ship.render(this.ctx, this.game, this.winner);
    this.segments.forEach((item) => {
      item.render(this.ctx);
    });
    this.particles.forEach((item) => {
      if (item.remaining_life < 0 || item.radius < 0) {
        item = new Particle(this.ship.x, this.ship.y);
      }
      item.render(this.ctx);
    });
  }

  /**
   * 
   */
  checkLanding() {
    if (this.ship.x > this.safe * this.distanceX && this.ship.x < (this.safe + 2) * this.distanceX
      && (this.ship.y + this.ship.size / 2) > this.points[this.safe].y) {
      return true;
    }
  }

  /**
   * 
   */
  checkBoundaries() {
    if (this.ship.x > right) {
      this.ship.x = right;
    } else if (this.ship.x < left) {
      this.ship.x = left;
    } else if (this.ship.y < top) {
      this.ship.y = top;
    }
  }

  /**
   * 
   * @param {*} segment 
   * @param {*} circle 
   */
  collisionSegmentCircle(segment, circle) {
    if (this.collisionCirclePoint(circle, { x: segment.x1, y: segment.y1 }) ||
      this.collisionCirclePoint(circle, { x: segment.x2, y: segment.y2 })) {
      this.game = false;
      this.winner = false;
      this.retro_txt.innerHTML = "¡Perdiste! :(";
      this.retro.style.display = 'block';
      return;
    }
    this.get_projection(segment, circle.x, circle.y);
    if (this.collisionCirclePoint(circle, this.projection) && this.projection.in_segment) {
      this.game = false;
      this.winner = false;
      this.retro_txt.innerHTML = "¡Perdiste! :(";
      this.retro.style.display = 'block';
      return;
    }
  }

  /**
   * 
   * @param {*} circle 
   * @param {*} point 
   */
  collisionCirclePoint(circle, point) {
    let dist = distance(circle.x, circle.y, point.x, point.y);
    return (dist < circle.radius) ? true : false;
  }

  /**
   * 
   * @param {*} segment 
   * @param {*} x 
   * @param {*} y 
   */
  get_projection(segment, x, y) {
    let segment_length = distance(segment.x1, segment.y1, segment.x2, segment.y2);

    let segment_unit_vector_x = (segment.x2 - segment.x1) / segment_length;
    let segment_unit_vector_y = (segment.y2 - segment.y1) / segment_length;

    let dot = dotProduct(segment_unit_vector_x, segment_unit_vector_y, x - segment.x1, y - segment.y1);

    this.projection.x = segment.x1 + dot * segment_unit_vector_x;
    this.projection.y = segment.y1 + dot * segment_unit_vector_y;

    this.projection.in_segment = ((dot >= 0) && (dot <= segment_length)) ? true : false;

    return this.projection;
  }

  /**
   * Función que reinicia el juego.
   */
  restart() {
    window.location.reload();
  }
}

/**
 * 
 * @param {*} x1 
 * @param {*} y1 
 * @param {*} x2 
 * @param {*} y2 
 */
function distance(x1, y1, x2, y2) {
  let dx = x2 - x1;
  let dy = y2 - y1;
  return Math.sqrt(dx * dx + dy * dy);
}

/**
 * 
 * @param {*} x1 
 * @param {*} y1 
 * @param {*} x2 
 * @param {*} y2 
 */
function dotProduct(x1, y1, x2, y2) {
  return x1 * x2 + y1 * y2;
}
