const KEY = {
  _pressed: new Map(),

  DOWN: "ArrowDown",
  LEFT: "ArrowLeft",
  RIGHT: "ArrowRight",

  /**
   * 
   * @param {*} key 
   */
  isPressed: function (key) {
    return this._pressed.get(key);
  },
  /**
   * 
   * @param {*} key 
   */
  onKeyDown: function (key) {
    this._pressed.set(key, true);
  },
  /**
   * 
   * @param {*} key 
   */
  onKeyUp: function (key) {
    this._pressed.delete(key);
  }
}

export default KEY;