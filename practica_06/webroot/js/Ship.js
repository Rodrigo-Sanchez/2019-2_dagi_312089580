import Sprite from "./Sprite.js";

const PI_180 = Math.PI / 180;
const drawing_angle = 125;
const ROTATE_ANGLE = 3;
const SHIP_FORCE = 20;
let gravity = 3;
let sin;
let cos;
let size_2;
let size_3;
let size_8;

export default class Ship extends Sprite {
  constructor(x = 0, y = 0, size = 1) {
    super(x, y, size, size, "webroot/img/space_ship.svg");

    this.x = x;
    this.y = y;
    this.size = size;
    this.rotation = 270 * PI_180;
    this.force = 0;
    this.friction = 0.95;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
    this.showFlame = false;
    this.down_arrow_pressed = false;
  }

  /**
   * 
   * @param {*} KEY 
   */
  processInput(KEY) {
    // Get the Object by ID
    let object = document.querySelector("#navigation");
    // Get the SVG document inside the Object tag
    let svgDoc = object.contentDocument;

    let left_arrow = svgDoc.querySelector("#left_arrow");
    let right_arrow = svgDoc.querySelector("#right_arrow");
    let down_arrow = svgDoc.querySelector("#down_arrow");

    down_arrow.addEventListener('mousedown', () => {
      this.increaseSpeed();
      this.down_arrow_pressed = true;
    });
    down_arrow.addEventListener('mouseup', () => {
      this.down_arrow_pressed = false;
    });

    left_arrow.addEventListener('mousedown', () => {
      this.velocity.x=50;
    });
    right_arrow.addEventListener('mousedown', () => {
      this.velocity.x=-50;
    });

    if (KEY.isPressed(KEY.LEFT)) {
      this.moveRight();
    }

    if (KEY.isPressed(KEY.RIGHT)) {
      this.moveLeft();
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.increaseSpeed();
    }
    if (!KEY.isPressed(KEY.DOWN) && !this.down_arrow_pressed) {
      this.zeroVelocity();
    }
  }

  /** 
   * 
   */
  moveRight() {
    this.velocity.x++;
  }

  /** 
   * 
   */
  moveLeft() {
    this.velocity.x--;
  }

  /** 
   * 
   */
  increaseSpeed() {
    this.force = SHIP_FORCE;
    this.showFlame = true;
  }

  /** 
   * 
   */
  zeroVelocity() {
    this.force = 0;
    this.showFlame = false;
  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {
    this.acceleration.x = Math.cos(this.rotation) * this.force;
    this.acceleration.y = Math.sin(this.rotation) * this.force;

    this.velocity.x += this.acceleration.x;
    this.velocity.y += this.acceleration.y;

    this.velocity.x *= this.friction;
    this.velocity.y *= this.friction;

    this.x += this.velocity.x * elapsed;
    this.y += this.velocity.y * elapsed;
    this.y += gravity;
  }

  /**
   * 
   * @param {*} ctx 
   */
  render(ctx, game, winner) {
    cos = Math.cos(drawing_angle * PI_180);
    sin = Math.sin(drawing_angle * PI_180);

    size_2 = this.size / 2;
    size_3 = this.size / 3;
    size_8 = this.size / 8;

    if(game || winner) {
      super.render(ctx);
    } else {
      super.render(ctx, 270);
    }

    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.rotation);

    if (this.showFlame) {
      ctx.fillStyle = "#e74c3c";
      ctx.beginPath();
      ctx.moveTo(-size_2, 0);
      ctx.lineTo(size_2 * cos, size_2 * sin);
      ctx.lineTo(-size_8, 0);
      ctx.lineTo(size_2 * cos, -size_2 * sin);
      ctx.closePath();
      ctx.fill();
    }

    ctx.restore();
  }
}
