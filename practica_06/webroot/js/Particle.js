export default class Particle {

  constructor(pos_x = 0, pos_y = 0) {
    this.x = pos_x;
    this.y = pos_y;
    this.speed = { x: -2.5 + Math.random() * 5, y: -15 + Math.random() * 10 };
    this.radius = 10 + Math.random() * 20;

    this.life = 20 + Math.random() * 10;
    this.remaining_life = this.life;

    this.r = Math.round(Math.random() * 3);
    this.g = Math.round(Math.random() * 3);
    this.b = Math.round(Math.random() * 3);
  }

  /**
   * 
   * @param {*} elapsed 
   * @param {*} pos_x 
   * @param {*} pos_y 
   */
  update(elapsed, pos_x, pos_y) {
    this.x = pos_x;
    this.y = pos_y;
    this.x += this.speed.x;
    this.y += this.speed.y;
    this.radius--;
    this.remaining_life--;
  }

  /**
   * 
   * @param {*} ctx 
   */
  render(ctx) {
    ctx.beginPath();

    this.opacity = Math.round(this.remaining_life / this.life * 100) / 100;
    
    let gradient = ctx.createRadialGradient(this.x, this.y, 0, this.x, this.y, this.radius);
    gradient.addColorStop(0, "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.opacity + ")");
    gradient.addColorStop(0.5, "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.opacity + ")");
    gradient.addColorStop(1, "rgba(" + this.r + ", " + this.g + ", " + this.b + ", 0)");

    ctx.fillStyle = gradient;
    ctx.arc(this.x, this.y, this.radius, Math.PI * 2, false);
    ctx.fill();
  }
}