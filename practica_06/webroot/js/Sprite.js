export default class Sprite {
  constructor(x, y, w, h, img_path) {
    this.x = x;
    this.y = y;
    this.vx = 0;
    this.vy = 0;
    this.ax = 0;
    this.ay = 0;

    this.w = w;
    this.h = h;
    this.rotation = 0;

    this.image = null;
    if (img_path) {
      this.image = new Image();
      this.image.src = img_path;
    }
  }

  /**
   * 
   */
  processInput() { 
    
  }

  /**
   * 
   * @param {*} elapsed 
   */
  update(elapsed) {
    this.vx += this.ax;
    this.vy += this.ay;

    this.vx *= this.friction;
    this.vy *= this.friction;

    this.x += this.vx * elapsed;
    this.y += this.vy * elapsed;
  }

  /**
   * 
   * @param {*} ctx 
   */
  render(ctx) {
    if (this.image) {
      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.rotation + 90 * Math.PI / 180);
      ctx.drawImage(this.image, -this.w / 2, -this.h / 2, this.w, this.h);
      ctx.restore();
    }
  }

  /**
   * 
   * @param {*} ctx 
   * @param {*} rotate 
   */
  render(ctx, rotate) {
    if (this.image) {
      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.rotation + rotate * Math.PI / 180);
      ctx.drawImage(this.image, -this.w / 2, -this.h / 2, this.w, this.h);
      ctx.restore();
    }
  }
}